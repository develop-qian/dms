package com.manage.dms;

import com.alibaba.fastjson.JSON;
import com.manage.dms.common.utils.Constants;
import com.manage.dms.common.utils.EncodeMD5;
import com.manage.dms.common.utils.IdGen;
import com.manage.dms.common.utils.StringUtils;
import com.manage.dms.domain.model.User;
import com.manage.dms.service.IUserService;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;

import javax.annotation.Resource;

/**
 * Created by idiot on 2018/5/5.
 */
public class UserServiceTest extends AppTest {
    private static Logger logger = LoggerFactory.getLogger(UserServiceTest.class);

    @Resource
    private IUserService userService;

    /**
     * 通用新增方法测试
     */
    @Test
    public void insertUser(){
        /*User user = new User();
        user.setUserId(IdGen.get().nextId());
        user.setUserName("test1");
        user.setPassword(EncodeMD5.GetMD5Code("123456"));
        userService.insert(user);*/
    }


    /**
     * 通用修改方法测试
     */
    @Test
    public void updateUser(){
        /*User user = new User();
        user.setUserId(992707345354063872L);
        user.setRemark(StringUtils.generateUUID());
        userService.update(user);*/
    }

    @Test
    public void getUserById(){
        User user = userService.getInfoById(993744661916942336L);
        logger.error(JSON.toJSONString(user));
    }

    @Test
    public void getUserByTokenTest() throws Exception {
        /*String token = login("test1", "123456");
        logger.error("token: " + token);
        mvc.perform(MockMvcRequestBuilders.post("/api/user/getUserByToken.json")
                .header(Constants.Regular.TOKEN, token)
                .contentType(MediaType.APPLICATION_JSON_UTF8))
                .andDo(MockMvcResultHandlers.print());*/
    }
}
