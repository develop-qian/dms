package com.manage.dms;

import com.manage.dms.common.utils.Base64Util;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static org.junit.Assert.*;

/**
 * Created by idiot on 2018/4/29.
 */
public class Base64UtilTest {
    private static Logger logger = LoggerFactory.getLogger(Base64UtilTest.class);

    @Test
    public void test(){
        String base64str = Base64Util.encodeBase64("root", null);
        logger.info(base64str);
        logger.info(Base64Util.decodeBase64(base64str, null));
    }

}