package com.manage.dms;

import com.alibaba.fastjson.JSON;
import com.github.pagehelper.Page;
import com.manage.dms.common.utils.IdGen;
import com.manage.dms.domain.model.Model;
import com.manage.dms.domain.model.response.ResponseEntity;
import com.manage.dms.service.IModelService;
import com.manage.dms.web.api.ModelController;
import org.apache.ibatis.session.RowBounds;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Resource;

public class ModelServiceTest extends AppTest {
    private static Logger logger = LoggerFactory.getLogger(ModelServiceTest.class);

    @Resource
    private IModelService modelService;

    /**
     * 通用新增方法测试
     */
    @Test
    public void insertModel() {
        /*Model model = new Model();
        model.setModelId(IdGen.get().nextId());
        model.setModelName("test");
        model.setProjectId(Long.valueOf(006777));
        modelService.insert(model);*/
    }

    @Test
    public void updateModel() {
       /* Model model = new Model();
        model.setModelId(993118376400257024L);
        model.setModelName("test333");
        model.setProjectId(Long.valueOf(006777));
        modelService.update(model);*/
    }

    @Test
    public void deleteModel() {
        /*Long id = 993118376400257024L;
        modelService.delete(id);*/
    }

    @Test
    public void getInfo() {
        Long id = 993118376400257024L;
        Model model = modelService.getInfoById(id);
        logger.error(JSON.toJSONString(model));
    }

    @Test
    public void testCasePageModel() {
        Page<Model> page = modelService.queryModelByPage(993762352958537728L, new RowBounds(0, 1));
        logger.error(JSON.toJSONString(page));
    }

}
