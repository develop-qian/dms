package com.manage.dms;

import com.alibaba.fastjson.JSON;
import com.github.pagehelper.Page;
import com.manage.dms.common.utils.IdGen;
import com.manage.dms.domain.model.UserProject;
import com.manage.dms.service.IUserProjectService;
import org.apache.ibatis.session.RowBounds;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Resource;


public class UserProjectServiceTest extends AppTest{
    private static Logger logger = LoggerFactory.getLogger(ModelServiceTest.class);

    @Resource
    private IUserProjectService userProjectService;

    @Test
    public void insertUserProject(){
       /* UserProject project = new UserProject();
        project.setProjectId(IdGen.get().nextId());
        project.setUserProjectId(5555L);
        project.setUserId(IdGen.get().nextId());
        userProjectService.insert(project);*/
    }

    @Test
    public void updateUserProject(){
       /* UserProject project = new UserProject();
        project.setProjectId(6666L);
        project.setUserProjectId(5555L);
        project.setUserId(7777L);
        userProjectService.update(project);*/
    }

    @Test
    public void queryUserProjectTest(){
        Long userId = 992707345354063872L;
        Page<UserProject> userProjects = userProjectService.queryUserProjectByPage(userId, new RowBounds(0, 1));
        logger.error(JSON.toJSONString(userProjects));
    }
}
