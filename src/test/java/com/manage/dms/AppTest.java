package com.manage.dms;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.manage.dms.common.utils.Constants;
import com.manage.dms.domain.model.User;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

/**
 * Created by idiot on 2018/2/6.
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class AppTest {
    private static Logger logger = LoggerFactory.getLogger(AppTest.class);

    @Autowired
    protected WebApplicationContext context;
    protected MockMvc mvc;

    @Before
    public void setUp() throws Exception {
        //       mvc = MockMvcBuilders.standaloneSetup(new TestController()).build();
        mvc = MockMvcBuilders.webAppContextSetup(context).build();//建议使用这种
    }

    @Test
    public void test(){
        logger.error("......");
    }


    // 登录
    public String login(String userName, String password) throws Exception {
        MvcResult result = mvc.perform(MockMvcRequestBuilders.post("/api/user/login.json")
                .param("userName", userName)
                .param("password", password)
                .contentType(MediaType.APPLICATION_JSON))
                .andDo(MockMvcResultHandlers.print())
                .andReturn();
        JSONObject jsonObject = JSON.parseObject(result.getResponse().getContentAsString());
        return jsonObject.getJSONObject("data").getString(Constants.Regular.TOKEN);
    }
}
