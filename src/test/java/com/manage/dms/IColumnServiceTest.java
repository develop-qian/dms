package com.manage.dms;

import com.alibaba.fastjson.JSON;
import com.github.pagehelper.Page;
import com.manage.dms.common.utils.IdGen;
import com.manage.dms.common.utils.JsonUtils;
import com.manage.dms.domain.model.Column;
import com.manage.dms.domain.model.Model;
import com.manage.dms.domain.model.response.ResponseEntity;
import com.manage.dms.service.IColumnService;
import com.manage.dms.web.api.ColumnController;
import org.apache.ibatis.session.RowBounds;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Resource;

public class IColumnServiceTest extends AppTest{
    private static Logger logger = LoggerFactory.getLogger(IColumnServiceTest.class);

    @Resource
    private IColumnService columnService;

    @Test
    public void insertColumn(){
        /*Column column = new Column();
        column.setColumnId(IdGen.get().nextId());
        column.setModelId(IdGen.get().nextId());
        column.setColumnName("字段1");
        column.setColumnType("aaaa");
        columnService.insert(column);*/
    }

    @Test
    public void updateColumn(){
       /* Column column = new Column();
        column.setColumnId(993134190943797248L);
        column.setModelId(IdGen.get().nextId());
        column.setColumnName("字段12222");
        column.setColumnType("aaaa");
        columnService.update(column);*/
    }

    @Test
    public void getColumns(){
        Page<Column> page =  columnService.queryColumnByPage(993428253949558784L, new RowBounds());
        logger.info(JSON.toJSONString(JsonUtils.parseArray(page)));
    }


}
