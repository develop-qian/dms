package com.manage.dms;

import com.manage.dms.common.utils.StringUtils;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by Administrator on 2018/5/5.
 */
public class StringUtilsTest {
    private static Logger logger = LoggerFactory.getLogger(StringUtilsTest.class);

    @Test
    public void testCamelCaseToUnderLineCase(){
        String param = "userId";
        String res = StringUtils.camelCaseToUnderLineCase(param);
        logger.error(res);
    }
}