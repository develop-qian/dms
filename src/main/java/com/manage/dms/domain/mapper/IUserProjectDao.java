package com.manage.dms.domain.mapper;

import com.github.pagehelper.Page;
import com.manage.dms.domain.model.User;
import com.manage.dms.domain.model.UserProject;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.session.RowBounds;

import java.util.List;

/**
 * 用户工程表
 */
@Mapper
public interface IUserProjectDao {

    /**
     * 根据用户Id查询用户工程表
     * @param tableName
     * @param userId
     * @param rowBounds
     * @return
     */
    @Select("select * from ${tableName} where user_id = #{userId}")
    Page<UserProject> queryUserProjectByPage(@Param("tableName") String tableName, @Param("userId") Long userId, RowBounds rowBounds);

    // 查询userProject信息
    @Select("select * from ${tableName} where user_id = #{userId} and project_id = #{projectId}")
    UserProject getInfoByUserIdAndProjectId(@Param("tableName") String tableName, @Param("userId") Long userId, @Param("projectId") Long projectId);
}
