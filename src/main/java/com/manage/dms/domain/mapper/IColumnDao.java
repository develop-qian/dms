package com.manage.dms.domain.mapper;

import com.github.pagehelper.Page;
import com.manage.dms.domain.model.Column;
import com.manage.dms.domain.model.Model;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.session.RowBounds;

import java.util.List;

/**
 * 字段信息接口
 */
@Mapper
public interface IColumnDao {

    //根据项目Id删除Column
    @Delete("delete from ${tableName} where model_Id = #{modelId}")
    void deleteColumnByModelId(@Param("tableName") String tableName, @Param("modelId") Long modelId);

    //根据ModelID查询字段
    @Select("select * from ${tableName} where model_Id = #{modelId}")
    Page<Column> queryColumnByPage(@Param("tableName") String tableName, @Param("modelId") Long modelId, @Param("rowBounds") RowBounds rowBounds);

    //根据ModelId查询Column
    @Select("select * from ${tableName} where model_Id = #{modelId}")
    List<Column> queryColumnByModelId(@Param("tableName") String tableName, @Param("modelId") Long modelId);
}
