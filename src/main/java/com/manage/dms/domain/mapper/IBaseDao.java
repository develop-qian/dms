package com.manage.dms.domain.mapper;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.Map;

/**
 * Created by idiot on 2018/5/5.
 */
@Mapper
public interface IBaseDao {
    /**
     * 新增
     * @param tableName
     * @param titleMap
     * @param paramMap
     * @return
     */
    int insert(@Param("tableName") String tableName, @Param("titles") Map titleMap, @Param("params") Map paramMap);

    /**
     * 修改
     * @param tableName
     * @param id
     * @param columnMap
     * @return
     */
    int update(@Param("tableName") String tableName, @Param("primaryKey") String id, @Param("primaryKeyValue") Object idValue, @Param("columns") Map columnMap);


    /**
     * 根据主键删除
     * @param tableName
     * @param primaryKey
     * @param id
     * @return
     */
    @Delete("delete from ${tableName} where ${primaryKey} = #{id}")
    int deleteById(@Param("tableName") String tableName, @Param("primaryKey") String primaryKey, @Param("id") Long id);

    /**
     * 根据主键获取信息
     * @param tableName
     * @param primaryKey
     * @param id
     * @return
     */
    Map getInfoById(@Param("tableName") String tableName, @Param("primaryKey") String primaryKey, @Param("id") Long id);
}
