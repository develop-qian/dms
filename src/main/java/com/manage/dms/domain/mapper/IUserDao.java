package com.manage.dms.domain.mapper;

import com.manage.dms.domain.model.User;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

/**
 * Created by idiot on 2018/5/5.
 */
@Mapper
public interface IUserDao {

    // 根据用户名称密码获取用户信息
    @Select("select * from ${tableName} where user_name = #{userName} and password = #{password} ")
    User getUserByUserNameAndPassword(@Param("tableName") String tableName, @Param("userName") String userName, @Param("password") String password);
}
