package com.manage.dms.domain.mapper;

import com.github.pagehelper.Page;
import com.manage.dms.domain.model.Project;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.session.RowBounds;

import java.util.List;

/**
 * tjq
 * 查询项目
 */
@Mapper
public interface IProjectDao {

    @Select("select * from #{tableName} where project_Id = #{projectId}")
    List<Project> queryProjectByProjectId(@Param("tableName") String tableName, @Param("projectId") Long projectId);

    // 获取用户拥有的工程信息
    Page<Project> getUserProjects(@Param("tableName") String tableName, @Param("project") Project info, RowBounds rowBounds);
}
