package com.manage.dms.domain.mapper;

import com.github.pagehelper.Page;
import com.manage.dms.domain.model.Model;
import com.manage.dms.domain.model.UserProject;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.session.RowBounds;

import java.util.List;

/**
 * 实体查询
 */
@Mapper
public interface IModelDao {

    @Select("select * from ${tableName} where project_id = #{projectId}")
    Page<Model> queryModelByPage(@Param("tableName") String tableName, @Param("projectId") Long projectId, RowBounds rowBounds);

    @Select("select * from ${tableName} where project_id = #{projectId}")
    List<Model> queryModelByProjectId(@Param("tableName") String tableName, @Param("projectId") Long projectId);
}
