package com.manage.dms.domain.model.response;

import com.alibaba.fastjson.JSON;

/**
 * Created by idiot on 2018/5/6.
 * 异常返回信息实体
 * 若结果正常就返回需要的数据
 */
public class ResponseEntity {
    /**
     * 异常码
     */
    private Integer code;
    /**
     * 异常描述
     */
    private String msg;

    /**
     * 数据
     */
    private Object data;

    /**
     * 总页数
     */
    private Long total;


    public ResponseEntity() {
    }

    public ResponseEntity(Integer code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    /**
     * 返回数据
     * @param code
     * @param msg
     * @param data
     * @return
     */
    public static Object response(int code, String msg, Object data){
        ResponseEntity responseEntity = new ResponseEntity(code, msg);
        if (data != null)
            responseEntity.setData(data);
        return JSON.toJSONString(responseEntity);
    }


    /**
     * 返回数据
     * @param code
     * @param msg
     * @param data
     * @param total
     * @return
     */
    public static Object response(int code, String msg, Object data, Long total){
        ResponseEntity responseEntity = new ResponseEntity(code, msg);
        if (data != null)
            responseEntity.setData(data);
        responseEntity.setTotal(total);
        return JSON.toJSONString(responseEntity);
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }

    public Long getTotal() {
        return total;
    }

    public void setTotal(Long total) {
        this.total = total;
    }
}
