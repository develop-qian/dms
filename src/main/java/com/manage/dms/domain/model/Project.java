package com.manage.dms.domain.model;

import com.manage.dms.common.annotation.Table;

/**
 * 项目信息表
 */
@Table(tableName = "t_project", primaryKey = "project_id")
public class Project extends  BaseEntity{

    /**
     * 项目id
     */
    private Long projectId;

    /**
     * 项目名称
     */
    private String projectName;

    // 查询字段
    private Long userId;


    public Long getProjectId() {
        return projectId;
    }

    public void setProjectId(Long projectId) {
        this.projectId = projectId;
    }

    public String getProjectName() {
        return projectName;
    }

    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }

    /********************************查询字段****************************/
    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }
}
