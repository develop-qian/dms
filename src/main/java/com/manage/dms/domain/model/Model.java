package com.manage.dms.domain.model;

import com.manage.dms.common.annotation.Table;

/**
 * 实体信息表
 */
@Table(tableName = "t_model", primaryKey = "model_id")
public class Model extends BaseEntity{

    /**
     * 实体信息id
     */
    private Long modelId;

    /**
     * 项目id
     */
    private Long projectId;

    /**
     * 项目id
     */
    private String modelName;

    public Long getModelId() {
        return modelId;
    }

    public void setModelId(Long modelId) {
        this.modelId = modelId;
    }

    public Long getProjectId() {
        return projectId;
    }

    public void setProjectId(Long projectId) {
        this.projectId = projectId;
    }

    public String getModelName() {
        return modelName;
    }

    public void setModelName(String modelName) {
        this.modelName = modelName;
    }


}
