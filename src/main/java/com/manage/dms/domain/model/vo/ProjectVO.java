package com.manage.dms.domain.model.vo;

import com.manage.dms.domain.model.Project;

/**
 * Created by idiot on 2018/5/5.
 * vo 用于页面数据的展示
 * 作为model的拓展
 * 保证model跟数据库字段一致
 * 一些额外的查询字段  显示字段就放在vo里面
 */
public class ProjectVO extends Project{

}
