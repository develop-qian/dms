package com.manage.dms.domain.model;

import com.manage.dms.common.annotation.Table;

/**
 * 用户项目关联信息表
 */
@Table(tableName = "t_user_project", primaryKey = "user_project_id")
public class UserProject extends BaseEntity{

    /**
     * 用户项目关联id
     */
    private Long userProjectId;

    /**
     * 用户id
     */
    private Long userId;

    /**
     * 项目id
     */
    private Long projectId;


    public Long getUserProjectId() {
        return userProjectId;
    }

    public void setUserProjectId(Long userProjectId) {
        this.userProjectId = userProjectId;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Long getProjectId() {
        return projectId;
    }

    public void setProjectId(Long projectId) {
        this.projectId = projectId;
    }

}
