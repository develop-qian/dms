package com.manage.dms.domain.model;

import com.manage.dms.common.annotation.Table;

/**
 * 字段信息表
 */
@Table(tableName = "t_column", primaryKey = "column_id")
public class Column extends  BaseEntity{

    /**
     * 字段id
     */
    private Long columnId;

    /**
     * 实体id
     */
    private Long modelId;

    /**
     * 字段名称
     */
    private String columnName;

    /**
     * 字段类型
     */
    private String columnType;

    /**
     * 字段长度
     */
    private Integer columnLength;

    /**
     * 是否为空
     */
    private Integer nullFlag;

    /**
     * 默认值
     */
    private String defaultValue;

    /**
     * 是否是主键{1: 是, 0: 否}
     */
    private Integer primaryKeyFlag;

    /**
     * 保留小数位
     */
    private Integer decimalPlace;

    /**
     * 是否是外键{1: 是, 0: 否}
     */
    private Integer foreignKeyFlag;

    /**
     * 外键关联的实体id
     */
    private Long foreignModelId;

    /**
     * 外键关联的字段id
     */
    private Long foreignColumnId;

    public Long getColumnId() {
        return columnId;
    }

    public void setColumnId(Long columnId) {
        this.columnId = columnId;
    }

    public Long getModelId() {
        return modelId;
    }

    public void setModelId(Long modelId) {
        this.modelId = modelId;
    }

    public String getColumnName() {
        return columnName;
    }

    public void setColumnName(String columnName) {
        this.columnName = columnName;
    }

    public String getColumnType() {
        return columnType;
    }

    public void setColumnType(String columnType) {
        this.columnType = columnType;
    }

    public Integer getColumnLength() {
        return columnLength;
    }

    public void setColumnLength(Integer columnLength) {
        this.columnLength = columnLength;
    }

    public Integer getNullFlag() {
        return nullFlag;
    }

    public void setNullFlag(Integer nullFlag) {
        this.nullFlag = nullFlag;
    }

    public String getDefaultValue() {
        return defaultValue;
    }

    public void setDefaultValue(String defaultValue) {
        this.defaultValue = defaultValue;
    }

    public Integer getPrimaryKeyFlag() {
        return primaryKeyFlag;
    }

    public void setPrimaryKeyFlag(Integer primaryKeyFlag) {
        this.primaryKeyFlag = primaryKeyFlag;
    }

    public Integer getDecimalPlace() {
        return decimalPlace;
    }

    public void setDecimalPlace(Integer decimalPlace) {
        this.decimalPlace = decimalPlace;
    }

    public Integer getForeignKeyFlag() {
        return foreignKeyFlag;
    }

    public void setForeignKeyFlag(Integer foreignKeyFlag) {
        this.foreignKeyFlag = foreignKeyFlag;
    }

    public Long getForeignModelId() {
        return foreignModelId;
    }

    public void setForeignModelId(Long foreignModelId) {
        this.foreignModelId = foreignModelId;
    }

    public Long getForeignColumnId() {
        return foreignColumnId;
    }

    public void setForeignColumnId(Long foreignColumnId) {
        this.foreignColumnId = foreignColumnId;
    }
}
