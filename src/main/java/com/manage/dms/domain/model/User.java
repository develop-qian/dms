package com.manage.dms.domain.model;

import com.manage.dms.common.annotation.Table;

/**
 * 用户信息表
 */
@Table(tableName = "t_user", primaryKey = "user_id")
public class User extends  BaseEntity {

    /**
     * 用户信息id
     */
    private Long userId;

    /**
     * 用户名
     */
    private String userName;

    /**
     * 用户密码
     */
    private String password;

    /**
     * 类型{0: 用户, 1: 管理员}
     */
    private Integer kind;

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Integer getKind() {
        return kind;
    }

    public void setKind(Integer kind) {
        this.kind = kind;
    }

}
