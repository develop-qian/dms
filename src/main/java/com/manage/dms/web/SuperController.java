package com.manage.dms.web;

import com.manage.dms.common.exceptions.MyException;
import com.manage.dms.common.utils.Constants;
import com.manage.dms.common.utils.TokenUtil;
import com.manage.dms.domain.model.User;
import com.manage.dms.service.IUserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.text.SimpleDateFormat;
import java.util.Date;


/**
 * Created by idiot on 2017/6/15.
 */
@SuppressWarnings("JavaDoc")
public class SuperController {
    private static Logger logger = LoggerFactory.getLogger(SuperController.class);
    /**
     * @Description 作为基础的控制器，方便调用request，session，response
     *              用ThreadLocal 是因为防止  局部共享变量  导致数据污染
     */
    protected ThreadLocal<HttpServletRequest> request_local = new ThreadLocal<>();
    protected ThreadLocal<HttpServletResponse> response_local = new ThreadLocal<>();
    protected ThreadLocal<HttpSession> session_local = new ThreadLocal<>();

    @Resource
    private IUserService userService;



    @ModelAttribute
    public void setReqAndRes(HttpServletRequest request, HttpServletResponse response) {
        this.request_local.set(request);
        this.response_local.set(response);
        this.session_local.set(request.getSession());
    }

    @InitBinder
    public void initBinder(WebDataBinder binder) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        binder.registerCustomEditor(Date.class, new CustomDateEditor(dateFormat, true));
    }

    // 根据token获取用户信息编号
    protected Long getUserIdByToken(String token){
        Long userId = null;
        try {
            String decodeToken = TokenUtil.decodeToken(token.getBytes());
            String[] message = decodeToken.split(Constants.Regular.HASHTAG);
            userId = Long.parseLong(message[0]);
        } catch (Exception e) {
            logger.error("token解密失败.....");
            //抛出异常比较合适   抛出自定义异常   异常返回json 信息
            throw new MyException("token解密失败");
        }
        return userId;
    }

    // 获取用户
    protected User getUser(Long userId){
        if (userId == null)
            return null;
        User user = userService.getInfoById(userId);
        return user;
    }


    /**
     * @apiDefine result
     * @apiSuccess {Number} code 状态码
     * @apiSuccess {String} codeMsg 状态码描述信息
     * @apiSuccess {Object} data 数据
     *
     * @apiSuccessExample 参数异常请求返回示例:
     *     HTTP/1.1 200 OK
     *     {
     *       "code": 1000,
     *       "msg": "参数为空"
     *       "data": null
     *     }
     */



}