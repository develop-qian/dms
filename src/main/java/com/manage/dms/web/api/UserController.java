package com.manage.dms.web.api;

import com.manage.dms.common.annotation.NotNullParam;
import com.manage.dms.common.annotation.RequireToken;
import com.manage.dms.common.utils.*;
import com.manage.dms.domain.model.User;
import com.manage.dms.domain.model.response.ResponseEntity;
import com.manage.dms.service.IUserService;
import com.manage.dms.web.SuperController;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.HashMap;

/**
 * Created by idiot on 2018/4/29.
 */
@RestController
public class UserController extends SuperController {
    private static Logger logger = LoggerFactory.getLogger(UserController.class);

    @Resource
    private IUserService userService;

    /**
     * 登录接口
     * @param userName
     * @param password
     * @return
     * @author idiot
     *
     * @api {post} /api/user/login.json  登录接口
     * @apiVersion 1.0.0
     * @apiGroup user
     * @apiName login
     *
     * @apiParam {String} userName 用户名
     * @apiParam {String} password 密码
     *
     * @apiUse result
     * @apiSuccess {String} data.Token 用户token
     */
    @NotNullParam(params = {"userName", "password"})
    @PostMapping(value = "/api/user/login.json")
    public Object login(String userName, String password){
        logger.info("用户登录接口......");
        // 获取用户信息
        User user = userService.getUserByUserNameAndPassword(userName, EncodeMD5.GetMD5Code(password));
        if (user == null){
            return new ResponseEntity(Constants.ResponseCode.INVALID_RESULT, "信息不存在");
        }
        // 生成token
        String token = TokenUtil.encodeToken(user.getUserId() + Constants.Regular.HASHTAG + System.currentTimeMillis());
        HashMap<String, Object> map = new HashMap<>();
        map.put(Constants.Regular.TOKEN, token);
        // 返回值
        return ResponseEntity.response(Constants.ResponseCode.SUCCESS, "成功", map);
    }


    /**
     * 根据token获取用户信息接口
     * @param token
     * @return
     * @author idiot
     *
     * @api {post} /api/user/getUserByToken.json  根据token获取用户信息接口
     * @apiVersion 1.0.0
     * @apiGroup user
     * @apiName getUserByToken
     *
     * @apiHeader {String} Token 用户token
     *
     * @apiUse result
     * @apiSuccess {Number} data.userId 用户id
     * @apiSuccess {String} data.userName 用户名称
     * @apiSuccess {Number} data.kind 类型{0: 用户, 1: 管理员}
     */
    @RequireToken(value = true)
    @PostMapping(value = "/api/user/getUserByToken.json")
    public Object getUserByToken(@RequestHeader(value = Constants.Regular.TOKEN) String token){
        logger.info("根据token获取用户信息接口......");
        // 验证用户信息是否有效
        User user = getUser(getUserIdByToken(token));
        if(user == null){
            return new ResponseEntity(Constants.ResponseCode.INVALID_TOKEN, "无效的token");
        }
        // 置空密码  返回用户信息
        user.setPassword(null);
        return ResponseEntity.response(Constants.ResponseCode.SUCCESS, "成功", JsonUtils.parseObject(user));
    }

    /**
     * 编辑用户信息接口
     * @param token
     * @param info
     * @return
     * @author idiot
     *
     * @api {post} /api/user/updateUser.json  编辑用户信息接口
     * @apiVersion 1.0.0
     * @apiGroup user
     * @apiName updateUser
     *
     * @apiHeader {String} Token 用户token
     *
     * @apiParam {Number} [userId] 用户id 为空表示添加  不为空为修改
     * @apiParam {String} password 密码
     *
     * @apiUse result
     */
    @RequireToken(value = true)
    @PostMapping(value = "/api/user/updateUser.json")
    public Object updateUser(@RequestHeader(value = Constants.Regular.TOKEN) String token,
                             User info){
        logger.info("编辑用户信息接口......");
        // 验证用户信息是否有效
        User user = getUser(getUserIdByToken(token));
        if(user == null){
            return new ResponseEntity(Constants.ResponseCode.INVALID_TOKEN, "无效的token");
        }
        // 判断修改还是添加
        if (info.getUserId() == null){
            // 添加
            info.setUserId(IdGen.get().nextId());   //设置主键
            info.setCreateTime(System.currentTimeMillis());     //创建时间
            info.setCreateUser(user.getUserId());   //创建人
            userService.insert(info);
            return new ResponseEntity(Constants.ResponseCode.SUCCESS, "添加用户信息成功");
        }else {
            //修改
            info.setUpdateTime(System.currentTimeMillis());     // 修改时间
            info.setUpdateUser(user.getUserId());       // 修改人
            // 如果密码不为空  需要加密
            if (!StringUtils.isEmpty(info.getPassword())){
                info.setPassword(EncodeMD5.GetMD5Code(info.getPassword()));
            }
            userService.update(info);
            return new ResponseEntity(Constants.ResponseCode.SUCCESS, "修改用户信息成功");
        }
    }


    // 删除用户信息接口   暂时可能不需要



}
