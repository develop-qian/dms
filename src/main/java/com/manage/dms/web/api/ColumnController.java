package com.manage.dms.web.api;

import com.github.pagehelper.Page;
import com.manage.dms.common.annotation.NotNullParam;
import com.manage.dms.common.annotation.RequireToken;
import com.manage.dms.common.utils.Constants;
import com.manage.dms.common.utils.IdGen;
import com.manage.dms.common.utils.JsonUtils;
import com.manage.dms.domain.model.Column;
import com.manage.dms.domain.model.User;
import com.manage.dms.domain.model.response.ResponseEntity;
import com.manage.dms.service.IColumnService;
import com.manage.dms.web.SuperController;
import org.apache.ibatis.session.RowBounds;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * Created by idiot on 2018/4/29.
 */
@RestController
public class ColumnController extends SuperController {
    private static org.slf4j.Logger logger = LoggerFactory.getLogger(ColumnController.class);

    @Resource
    private IColumnService columnService;


    /**
     * 编辑字段接口
     * @param token
     * @param column
     * @return
     * @author tjq
     *
     * @api {post} /api/column/update.json 编辑字段接口
     * @apiVersion 1.0.0
     * @apiGroup column
     * @apiName update
     *
     * @apiHeader {String} token 用户token
     *
     * @apiParam {Number} [columnId] 字段ID 若有值就是修改 没有值就是新增
     * @apiParam {Number} modelId 实体id 必填
     * @apiParam {String} columnName 字段名称
     * @apiParam {String} columnType 字段类型  -- 可选值 varchar  int  bigint .....
     * @apiParam {Number} columnLength 字段长度
     * @apiParam {Number} nullFlag 是否为空
     * @apiParam {String} defaultValue 默认值
     * @apiParam {Number} primaryKeyFlag 是否是主键
     * @apiParam {Number} decimalPlace 保留小数位
     * @apiParam {Number} foreignKeyFlag 是否是外键{1: 是, 0: 否}
     * @apiParam {Number} foreignModelId 外键关联的实体id
     * @apiParam {Number} foreignColumnId 外键关联的字段id
     *
     * @apiUse result
     * @apiSuccess {Number} data.columnId 字段id
     */
    @RequireToken(value = true)
    @NotNullParam(params = {"columnName", "modelId"})
    @PostMapping(value = "/api/column/update.json")
    public Object update(@RequestHeader(value = Constants.Regular.TOKEN)String token,
                         Column column) {
        logger.info("编辑字段接口......");
        User user = getUser(getUserIdByToken(token));
        if (user == null) {
            return new ResponseEntity(Constants.ResponseCode.INVALID_TOKEN, "无效的token");
        }
        // 判断修改还是添加
        if(null != column && null != column.getColumnId()){
            // 修改
            column.setUpdateUser(user.getUserId());
            column.setUpdateTime(System.currentTimeMillis());
            columnService.update(column);
            return ResponseEntity.response(Constants.ResponseCode.SUCCESS, "修改字段信息成功", String.valueOf(column.getColumnId()));
        }else{
            // 添加
            column.setColumnId(IdGen.get().nextId());
            column.setCreateUser(user.getUserId());
            column.setCreateTime(System.currentTimeMillis());
            columnService.insert(column);
            return ResponseEntity.response(Constants.ResponseCode.SUCCESS, "添加字段信息成功", String.valueOf(column.getColumnId()));
        }
    }


    /**
     * 删除字段接口
     * @param columnId
     * @return
     * @author tjq
     *
     * @api {post} /api/column/deleteByColumnId.json 删除字段接口
     * @apiVersion 1.0.0
     * @apiGroup column
     * @apiName delete
     *
     * @apiHeader {String} token 用户token
     *
     * @apiParam {Long} columnId 字段ID
     *
     * @apiUse result
     */
    @RequireToken(value = true)
    @NotNullParam(params = {"columnId"})
    @PostMapping(value = "/api/column/deleteByColumnId.json")
    public Object deleteByColumnId(@RequestHeader(value = Constants.Regular.TOKEN)String token,
                                   Long columnId) {
        logger.info("删除字段接口......");
        User user = getUser(getUserIdByToken(token));
        if (user == null) {
            return new ResponseEntity(Constants.ResponseCode.INVALID_TOKEN, "无效的token");
        }
        columnService.delete(columnId);
        return new ResponseEntity(Constants.ResponseCode.SUCCESS, "删除字段信息成功");
    }

    /**
     * 根据modelId分页获取字段接口
     * @param modelId 实体Id
     * @param pageNo 页码
     * @param pageSize 条数
     * @return
     * @author tjq
     *
     * @api {post} /api/column/queryColumnByPage.json 根据modelId分页获取字段接口
     * @apiVersion 1.0.0
     * @apiGroup column
     * @apiName queryColumnByPage
     *
     * @apiHeader {String} token 用户token
     *
     * @apiParam {Long} modelId 实体Id
     * @apiParam {Integer} pageNo 页码
     * @apiParam {Integer} pageSize 条数
     *
     * @apiUse result
     */
    @RequireToken(value = true)
    @NotNullParam(params = {"modelId","pageNo","pageSize"})
    @PostMapping(value = "/api/column/queryColumnByPage.json")
    public Object queryColumnByPage(@RequestHeader(value = Constants.Regular.TOKEN)String token,
                                    Long modelId,
                                    @RequestParam(defaultValue = "1") Integer pageNo,
                                    @RequestParam(defaultValue = "10") Integer pageSize) {
        logger.info("根据modelId分页获取字段接口......");
        User user = getUser(getUserIdByToken(token));
        if (user == null) {
            return new ResponseEntity(Constants.ResponseCode.INVALID_TOKEN, "无效的token");
        }
        ResponseEntity response = new ResponseEntity();
        Page<Column> page = columnService.queryColumnByPage(modelId, new RowBounds((pageNo - 1) * pageSize, pageSize));
        response.setTotal(page.getTotal());     // 设置总页数
        response.setData(JsonUtils.parseArray(page));
        response.setMsg("SUCCESS");
        response.setCode(Constants.ResponseCode.SUCCESS);
        return response;
    }

}
