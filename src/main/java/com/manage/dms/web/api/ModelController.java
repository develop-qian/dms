package com.manage.dms.web.api;

import com.github.pagehelper.Page;
import com.manage.dms.common.annotation.NotNullParam;
import com.manage.dms.common.annotation.RequireToken;
import com.manage.dms.common.utils.Constants;
import com.manage.dms.common.utils.IdGen;
import com.manage.dms.common.utils.JsonUtils;
import com.manage.dms.domain.model.Column;
import com.manage.dms.domain.model.Model;
import com.manage.dms.domain.model.User;
import com.manage.dms.domain.model.response.ResponseEntity;
import com.manage.dms.domain.model.vo.ModelVO;
import com.manage.dms.service.IColumnService;
import com.manage.dms.service.IModelService;
import com.manage.dms.web.SuperController;
import org.apache.ibatis.session.RowBounds;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

/**
 * Created by idiot on 2018/4/29.
 */
@RestController
public class ModelController extends SuperController {
    private static org.slf4j.Logger logger = LoggerFactory.getLogger(ModelController.class);

    @Resource
    private IModelService modelService;
    @Resource
    private IColumnService columnService;

    /**
     * 编辑model信息接口
     * @param token 用户Token
     * @param model
     * @return
     * @author tjq
     *
     * @api {post} /api/model/updateModel.json  编辑model信息接口
     * @apiVersion 1.0.0
     * @apiGroup model
     * @apiName updateModel
     *
     * @apiHeader {String} Token 用户token
     *
     * @apiParam {Number} [modelId] 实体ID 若有值就是修改 没有值就是新增
     * @apiParam {Number} projectId 工程id
     * @apiParam {String} modelName 实体名称
     *
     * @apiUse result
     * @apiSuccess {Number} data.modelId 实体id
     */
    @RequireToken(value = true)
    @NotNullParam(params = {"modelName", "projectId"})
    @PostMapping(value = "/api/model/updateModel.json")
    public Object updateModel(@RequestHeader(value = Constants.Regular.TOKEN) String token,
                              Model model) {
        logger.info("编辑model信息接口......");
        User user = getUser(getUserIdByToken(token));
        if (user == null) {
            return new ResponseEntity(Constants.ResponseCode.INVALID_TOKEN, "无效的token");
        }
        if (null != model && null != model.getModelId()) {
            model.setUpdateTime(System.currentTimeMillis());
            model.setUpdateUser(user.getUserId());
            modelService.update(model);
            return ResponseEntity.response(Constants.ResponseCode.SUCCESS, "修改成功", String.valueOf(model.getModelId()));
        } else {
            model.setModelId(IdGen.get().nextId()); // 设置主键
            model.setCreateUser(user.getUserId());  // 设置创建人
            model.setCreateTime(System.currentTimeMillis()); // 设置修改时间
            modelService.insert(model);
            return ResponseEntity.response(Constants.ResponseCode.SUCCESS, "添加成功", String.valueOf(model.getModelId()));
        }
    }

    /**
     * 删除model信息接口   需要同时删除表对应的字段
     *
     * @param modelId
     * @return
     * @author tjq
     *
     * @api {post} /api/model/deleteModel.json  删除model信息接口
     * @apiVersion 1.0.0
     * @apiGroup model
     * @apiName deleteModel
     *
     * @apiHeader {String} Token 用户token
     *
     * @apiParam {Number} modelId 实体id
     *
     * @apiUse result
     */
    @RequireToken(value = true)
    @NotNullParam(params = {"modelId"})
    @PostMapping(value = "/api/model/deleteModel.json")
    public Object deleteModel(@RequestHeader(value = Constants.Regular.TOKEN) String token,
                              Long modelId) {
        logger.info("删除model信息接口");
        User user = getUser(getUserIdByToken(token));
        if (user == null) {
            return new ResponseEntity(Constants.ResponseCode.INVALID_TOKEN, "无效的token");
        }
        columnService.deleteColumnByModelId(modelId);//删除字段
        modelService.delete(modelId);//删除实体表
        return new ResponseEntity(Constants.ResponseCode.SUCCESS, "SUCCESS");
    }


    /**
     * 根据projectId查询表信息接口
     *
     * @param projectId
     * @param pageNo
     * @param pageSize
     * @return
     * @author tjq
     *
     * @api {post} /api/model/queryModelByPage.json 根据projectId查询实体信息接口
     * @apiVersion 1.0.0
     * @apiGroup model
     * @apiName queryModelByPage
     *
     * @apiHeader {String} Token 用户token
     *
     * @apiParam {Number} projectId 项目Id
     * @apiParam {Number} pageNo 页码
     * @apiParam {Number} pageSize 条数
     *
     * @apiUse result
     */
    @RequireToken(value = true)
    @NotNullParam(params = {"projectId"})
    @PostMapping(value = "/api/model/queryModelByPage.json")
    public Object queryModelByPage(@RequestHeader(value = Constants.Regular.TOKEN) String token,
                                   Long projectId,
                                   @RequestParam(defaultValue = "1") Integer pageNo,
                                   @RequestParam(defaultValue = "10") Integer pageSize) {
        logger.info("根据projectId查询实体信息接口......");
        User user = getUser(getUserIdByToken(token));
        if (user == null) {
            return new ResponseEntity(Constants.ResponseCode.INVALID_TOKEN, "无效的token");
        }
        ResponseEntity response = new ResponseEntity();
        Page<Model> models = modelService.queryModelByPage(projectId, new RowBounds((pageNo - 1) * pageSize, pageSize));
        response.setTotal(models.getTotal());     // 设置总页数
        response.setData(JsonUtils.parseArray(models));
        response.setCode(Constants.ResponseCode.SUCCESS);
        response.setMsg("SUCCESS");
        return response;
    }

    /**
     * 根据modelId 获取model详情接口
     * @param token
     * @param modelId
     * @return
     * @author idiot
     *
     * @api {post} /api/model/getModelByModelId.json 根据modelId获取model详情接口
     * @apiVersion 1.0.0
     * @apiGroup model
     * @apiName getModelByModelId
     *
     * @apiHeader {String} Token 用户token
     *
     * @apiParam {Number} modelId 项目Id
     *
     * @apiUse result
     */
    @RequireToken(value = true)
    @NotNullParam(params = {"modelId"})
    @PostMapping(value = "/api/model/getModelByModelId.json")
    public Object getModelByModelId(@RequestHeader(value = Constants.Regular.TOKEN) String token,
                                    Long modelId) {
        logger.info("根据modelId 获取model详情接口......");
        User user = getUser(getUserIdByToken(token));
        if (user == null) {
            return new ResponseEntity(Constants.ResponseCode.INVALID_TOKEN, "无效的token");
        }
        // 查询model
        ModelVO modelVO = (ModelVO) modelService.getInfoById(modelId);
        if (modelVO == null){
            logger.error("根据modelId 获取model详情接口......无效的modelId: " + modelId);
            return ResponseEntity.response(Constants.ResponseCode.INVALID_PARAM, "无效的modelId", null);
        }
        // 查询column信息
        List<Column> columns = columnService.queryColumnByModelId(modelId);
        modelVO.setColumns(columns);
        return ResponseEntity.response(Constants.ResponseCode.SUCCESS, "成功", modelVO);
    }
}
