package com.manage.dms.web.api;

import com.github.pagehelper.Page;
import com.manage.dms.common.annotation.NotNullParam;
import com.manage.dms.common.annotation.RequireToken;
import com.manage.dms.common.utils.Constants;
import com.manage.dms.common.utils.IdGen;
import com.manage.dms.domain.model.Project;
import com.manage.dms.domain.model.User;
import com.manage.dms.domain.model.UserProject;
import com.manage.dms.domain.model.response.ResponseEntity;
import com.manage.dms.service.IProjectService;
import com.manage.dms.service.IUserProjectService;
import com.manage.dms.web.SuperController;
import org.apache.ibatis.session.RowBounds;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

import static org.apache.shiro.web.filter.mgt.DefaultFilter.user;

/**
 * 用户工程类
 */
@RestController
public class UserProjectController extends SuperController{
    private static Logger logger = LoggerFactory.getLogger(UserProjectController.class);

    @Resource
    private IUserProjectService userProjectService;
    @Resource
    private IProjectService projectService;

    /**
     * 查询用户工程
     * @param token
     * @param pageNo
     * @param pageSize
     * @return
     * @author tjq
     *
     * @apiIgnore
     * @api {post} /api/userProject/queryUserProjectByPage.json  分页查询用户工程接口
     * @apiVersion 1.0.0
     * @apiGroup userProject
     * @apiName queryUserProjectByPage
     *
     * @apiHeader {String} Token 用户token
     *
     * @apiParam {String} token 用户token
     * @apiParam {Integer} pageNo 页码
     * @apiParam {Integer} pageSize 条数
     *
     * @apiUse result
     */
    /*@RequireToken(value = true)
    @GetMapping(value = "/api/userProject/queryUserProjectByPage.json")
    public Object queryUserProjectAll(@RequestHeader(value = Constants.Regular.TOKEN) String token,
                                      @RequestParam(defaultValue = "1") Integer pageNo,
                                      @RequestParam(defaultValue = "10") Integer pageSize){
        logger.info("分页查询用户工程接口......");
        ResponseEntity response = new ResponseEntity();
        // 验证用户信息是否有效
        User user = getUser(getUserIdByToken(token));
        if (user == null) {
            return new ResponseEntity(Constants.ResponseCode.INVALID_TOKEN, "无效的token");
        }
        Page<UserProject> userProjects = userProjectService.queryUserProjectByPage(user.getUserId(), new RowBounds((pageNo - 1) * pageSize, pageSize));
        response.setData(userProjects);
        response.setCode(Constants.ResponseCode.SUCCESS);
        response.setMsg("SUCCESS");
        return response;
    }*/

    /**
     * 编辑用户工程接口
     * @param token
     * @param info
     * @return
     * @author tjq
     *
     * @apiIgnore
     * @api {post} /api/userProject/updateUserProject.json  编辑用户工程接口
     * @apiVersion 1.0.0
     * @apiGroup userProject
     * @apiName updateUserProject
     *
     * @apiHeader {String} Token 用户token
     *
     * @apiParam {String} token 用户token
     * @apiParam {Number} [userProjectId] 用户项目Id
     *
     * @apiUse result
     */
    /*@RequireToken(value = true)
    @PostMapping(value = "/api/userProject/updateUserProject.json")
    public Object updateUserProject(@RequestHeader(value = Constants.Regular.TOKEN) String token,
                                    UserProject info) {
        logger.info("编辑用户工程接口......");
        // 验证用户信息是否有效
        User user = getUser(getUserIdByToken(token));
        if (user == null) {
            return new ResponseEntity(Constants.ResponseCode.INVALID_TOKEN, "无效的token");
        }
        if (info.getUserProjectId() == null) { //添加
            info.setUserProjectId(IdGen.get().nextId());    // 设置主键
            info.setCreateUser(user.getUserId());   // 设置创建人
            userProjectService.insert(info);
            return new ResponseEntity(Constants.ResponseCode.SUCCESS, "添加用户信息成功");
        }else{ //修改
            info.setUpdateUser(user.getUserId());       // 设置修改人
            info.setUpdateTime(System.currentTimeMillis());     // 设置修改时间
            userProjectService.update(info);
            return new ResponseEntity(Constants.ResponseCode.SUCCESS, "修改用户信息成功");
        }
    }*/

    /**
     * 删除用户工程表
     * @param userProjectId
     * @return
     * @author tjq
     *
     * @apiIgnore
     * @api {post} /api/userProject/deleteUserProjectId.json  删除用户工程接口
     * @apiVersion 1.0.0
     * @apiGroup userProject
     * @apiName deleteUserProjectId
     *
     * @apiHeader {String} Token 用户token
     *
     * @apiParam {Long} userProjectId 用户项目Id
     *
     * @apiUse result
     */
    /*@RequireToken(value = true)
    @NotNullParam(params = {"userProjectId"})
    @PostMapping(value = "/api/userProject/deleteUserProjectId.json")
    public Object deleteUserProjectById(@RequestHeader(value = Constants.Regular.TOKEN) String token,
                                        Long userProjectId) {
        logger.info("删除用户工程接口......");
        // 验证用户信息是否有效
        User user = getUser(getUserIdByToken(token));
        if (user == null) {
            return new ResponseEntity(Constants.ResponseCode.INVALID_TOKEN, "无效的token");
        }
        UserProject userProject = userProjectService.getInfoById(userProjectId);
        List<Project> projects = projectService.queryProjectByProjectId(userProject.getProjectId());
        if(null != projects && projects.size() < 1){
            userProjectService.delete(userProjectId);
            return new ResponseEntity(Constants.ResponseCode.SUCCESS, "SUCCESS");
        }else{
            return new ResponseEntity(Constants.ResponseCode.MY_EXCEPTION, "项目下有工程，删除失败");
        }
    }*/
}
