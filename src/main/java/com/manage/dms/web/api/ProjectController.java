package com.manage.dms.web.api;

import com.github.pagehelper.Page;
import com.manage.dms.common.annotation.NotNullParam;
import com.manage.dms.common.annotation.RequireToken;
import com.manage.dms.common.utils.Constants;
import com.manage.dms.common.utils.IdGen;
import com.manage.dms.common.utils.JsonUtils;
import com.manage.dms.domain.model.Model;
import com.manage.dms.domain.model.response.ResponseEntity;
import com.manage.dms.domain.model.Project;
import com.manage.dms.domain.model.User;
import com.manage.dms.service.IModelService;
import com.manage.dms.service.IProjectService;
import com.manage.dms.web.SuperController;
import org.apache.ibatis.session.RowBounds;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

/**
 * Created by idiot on 2018/4/29.
 */
@RestController
public class ProjectController extends SuperController {
    private static Logger logger = LoggerFactory.getLogger(ProjectController.class);

    @Resource
    private IProjectService projectService;
    @Resource
    private IModelService modelService;

    /**
     * 编辑工程接口
     *
     * @param token
     * @param info
     * @return
     * @author
     *
     * @api {post} /api/project/updateProject.json  编辑工程接口
     * @apiVersion 1.0.0
     * @apiGroup project
     * @apiName updateProject
     *
     * @apiHeader {String} Token 用户token
     *
     * @apiParam {Number} [projectId] 工程id 若有值就是修改 没有值就是新增
     * @apiParam {String} projectName 工程名称
     *
     * @apiUse result
     * @apiSuccess {Number} projectId 工程id
     */
    @RequireToken(value = true)
    @NotNullParam(params = {"token"})
    @PostMapping(value = "/api/project/updateProject.json")
    public Object updateProject(@RequestHeader(value = Constants.Regular.TOKEN) String token,
                                Project info) {
        logger.info("编辑工程接口......");
        // 验证用户信息是否有效
        User user = getUser(getUserIdByToken(token));
        if (user == null) {
            return new ResponseEntity(Constants.ResponseCode.INVALID_TOKEN, "无效的token");
        }
        // 根据id判断添加或修改
        if (info.getProjectId() == null) {
            // 添加
            info.setProjectId(IdGen.get().nextId());    // 设置主键
            info.setCreateUser(user.getUserId());       // 设置创建人
            info.setCreateTime(System.currentTimeMillis());
            // 添加userProject  project  事务处理
            projectService.addUserProject(user.getUserId(), info);
            return ResponseEntity.response(Constants.ResponseCode.SUCCESS, "添加工程成功", String.valueOf(info.getProjectId()));
        } else {
            // 修改
            info.setUpdateUser(user.getUserId());   // 设置修改人
            info.setUpdateTime(System.currentTimeMillis()); // 设置修改时间
            projectService.update(info);
            return ResponseEntity.response(Constants.ResponseCode.SUCCESS, "修改工程成功", String.valueOf(info.getProjectId()));
        }
    }


    /**
     * 删除工程   若工程下面有表信息   不允许删除
     * @param projectId 项目Id
     * @return
     * @author tjq
     *
     * @api {post} /api/project/deleteById.json  删除工程接口
     * @apiVersion 1.0.0
     * @apiGroup project
     * @apiName deleteById
     *
     * @apiHeader {String} Token 用户token
     *
     * @apiParam {Long} data.projectId 项目Id
     * @apiUse result
     */
    @RequireToken(value = true)
    @NotNullParam(params = {"projectId"})
    @PostMapping(value = "/api/project/delete.json")
    public Object deleteById(@RequestHeader(value = Constants.Regular.TOKEN) String token,
                             Long projectId) {
        logger.info("删除工程接口......");
        // 验证用户信息是否有效
        User user = getUser(getUserIdByToken(token));
        if (user == null) {
            return new ResponseEntity(Constants.ResponseCode.INVALID_TOKEN, "无效的token");
        }
        //检查是否有实体表
        List<Model> models = modelService.queryModelByProjectId(projectId);
        if (null != models && models.size() < 1) {
            // 删除project 和 userProject  事务处理
            projectService.deleteUserProject(user.getUserId(), projectId);
            return new ResponseEntity(Constants.ResponseCode.SUCCESS, "SUCCESS");
        }else{
            return new ResponseEntity(Constants.ResponseCode.MY_EXCEPTION, "工程下面有表，删除失败");
        }
    }

    /**
     * 根据工程id获取工程信息
     *
     * @param projectId
     * @author tjq
     *
     * @api {post} /api/project/getProjectByProjectId.json  根据工程id获取工程信息接口
     * @apiVersion 1.0.0
     * @apiGroup project
     * @apiName getProjectByProjectId
     *
     * @apiHeader {String} Token 用户token
     *
     * @apiParam {String} projectId 项目Id
     *
     * @apiUse result
     *
     */
    @RequireToken(value = true)
    @NotNullParam(params = {"projectId"})
    @PostMapping(value = "/api/project/getProjectByProjectId.json")
    public Object getProjectByUserId(@RequestHeader(value = Constants.Regular.TOKEN) String token,
                                     Long projectId) {
        logger.info("根据工程id获取工程信息接口......");
        // 验证用户信息是否有效
        User user = getUser(getUserIdByToken(token));
        if (user == null) {
            return new ResponseEntity(Constants.ResponseCode.INVALID_TOKEN, "无效的token");
        }
        ResponseEntity response = new ResponseEntity();
        Project project = projectService.getInfoById(projectId);
        response.setData(JsonUtils.parseObject(project));
        response.setMsg("成功");
        response.setCode(Constants.ResponseCode.SUCCESS);
        return response;
    }

    /**
     * 获取用户的工程信息接口
     * @param token
     * @param pageNo
     * @param pageSize
     * @param info
     * @return
     * @author idiot
     *
     * @api {post} /api/project/getUserProjects.json  获取用户的工程信息接口
     * @apiVersion 1.0.0
     * @apiGroup project
     * @apiName getUserProjects
     *
     * @apiHeader {String} Token 用户token
     *
     * @apiParam {String} pageNo 页码
     * @apiParam {String} pageSize 条数
     * @apiParam {String} projectName 项目名称 模糊搜索
     *
     * @apiUse result
     */
    @RequireToken(value = true)
    @PostMapping(value = "/api/project/getUserProjects.json")
    public Object getUserProjects(@RequestHeader(value = Constants.Regular.TOKEN) String token,
                                  @RequestParam(defaultValue = "1") Integer pageNo,
                                  @RequestParam(defaultValue = "10") Integer pageSize,
                                  Project info){
        logger.info("获取用户的工程信息接口......");
        // 验证用户信息是否有效
        User user = getUser(getUserIdByToken(token));
        if (user == null) {
            return new ResponseEntity(Constants.ResponseCode.INVALID_TOKEN, "无效的token");
        }
        info.setUserId(user.getUserId());   // 设置userId
        // 获取用户拥有的工程信息
        Page<Project> list = projectService.getUserProjects(info, new RowBounds((pageNo - 1) * pageSize, pageSize));
        return ResponseEntity.response(Constants.ResponseCode.SUCCESS, "成功", JsonUtils.parseArray(list), list.getTotal());
    }

    /**
     * 生成用户Sql
     * @param token
     * @return
     * @author tjq
     *
     * @api {post} /api/project/generateSql.json  生成用户Sql接口
     * @apiVersion 1.0.0
     * @apiGroup project
     * @apiName getUserProjects
     *
     * @apiHeader {String} Token 用户token
     * @apiParam {String} projectId 项目Id
     * @apiUse result
     */
    @RequireToken(value = true)
    @PostMapping(value = "/api/project/generateSql.json")
    public Object generateSql(@RequestHeader(value = Constants.Regular.TOKEN) String token, Long projectId){
        logger.info("导出Sql接口......");
        // 验证用户信息是否有效
        User user = getUser(getUserIdByToken(token));
        if (user == null) {
            return new ResponseEntity(Constants.ResponseCode.INVALID_TOKEN, "无效的token");
        }
        projectService.generateSql(user.getUserId(),projectId);
        return null;
    }
}
