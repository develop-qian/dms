package com.manage.dms.service;

import com.github.pagehelper.Page;
import com.manage.dms.domain.model.Project;
import org.apache.ibatis.session.RowBounds;

import java.util.List;

/**
 * tjq
 * 项目操作
 */
public interface IProjectService extends IBaseService<Project> {

    //根据项目Id统计项目表数量
    List<Project> queryProjectByProjectId(Long projectId);

    // 添加userProject 和 project信息
    void addUserProject(Long userId, Project info);

    // 删除userProject 和 project
    void deleteUserProject(Long userId, Long projectId);

    // 获取用户拥有的工程信息
    Page<Project> getUserProjects(Project info, RowBounds rowBounds);

    //sql生成
    String generateSql(Long userId, Long projectId);
}
