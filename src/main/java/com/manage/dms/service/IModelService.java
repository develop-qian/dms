package com.manage.dms.service;

import com.github.pagehelper.Page;
import com.manage.dms.domain.model.Model;
import com.manage.dms.domain.model.UserProject;
import org.apache.ibatis.session.RowBounds;

import java.util.List;

public interface IModelService extends IBaseService<Model> {

    //分页获取Model
    Page<Model> queryModelByPage(Long projectId, RowBounds rowBounds);

    //根据项目Id统计实体表数量
    List<Model> queryModelByProjectId(Long projectId);

}
