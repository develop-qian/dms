package com.manage.dms.service;

import com.github.pagehelper.Page;
import com.manage.dms.domain.model.Column;
import org.apache.ibatis.session.RowBounds;

import java.util.List;

public interface IColumnService extends IBaseService<Column> {

    //根据modelId删除字段信息
    void deleteColumnByModelId(Long modelId);

    //分页获取Column
    Page<Column> queryColumnByPage(Long modelId, RowBounds rowBounds);

    //根据modelId查询字段信息
    List<Column>  queryColumnByModelId(Long modelId);
}