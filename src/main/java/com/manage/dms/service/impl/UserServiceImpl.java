package com.manage.dms.service.impl;

import com.manage.dms.common.annotation.Table;
import com.manage.dms.domain.mapper.IUserDao;
import com.manage.dms.domain.model.User;
import com.manage.dms.service.IUserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * Created by idiot on 2018/5/5.
 */
@Service
public class UserServiceImpl extends BaseServiceImpl<User> implements IUserService {
    private static Logger logger = LoggerFactory.getLogger(UserServiceImpl.class);
    @Resource
    private IUserDao userDao;

    @Override
    public User getUserByUserNameAndPassword(String userName, String password) {
        Table table = getTable();
        return userDao.getUserByUserNameAndPassword(table.tableName(), userName, password);
    }
}
