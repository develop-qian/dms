package com.manage.dms.service.impl;

import com.github.pagehelper.Page;
import com.manage.dms.common.annotation.Table;
import com.manage.dms.domain.mapper.IUserProjectDao;
import com.manage.dms.domain.model.UserProject;
import com.manage.dms.service.IUserProjectService;
import org.apache.ibatis.session.RowBounds;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

@Service
public class UserProjectImpl extends BaseServiceImpl<UserProject> implements IUserProjectService {

    @Resource
    private IUserProjectDao userProjectDao;

    @Override
    public Page<UserProject> queryUserProjectByPage(Long userId, RowBounds rowBounds) {
        Table table = getTable();
        return userProjectDao.queryUserProjectByPage(table.tableName(), userId, rowBounds);
    }

    @Override
    public UserProject getInfoByUserIdAndProjectId(Long userId, Long projectId) {
        Table table = super.getTable();
        return userProjectDao.getInfoByUserIdAndProjectId(table.tableName(), userId, projectId);
    }
}
