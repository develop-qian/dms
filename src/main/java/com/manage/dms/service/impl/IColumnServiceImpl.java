package com.manage.dms.service.impl;

import com.github.pagehelper.Page;
import com.manage.dms.common.annotation.Table;
import com.manage.dms.domain.mapper.IColumnDao;
import com.manage.dms.domain.model.Column;
import com.manage.dms.service.IColumnService;
import org.apache.ibatis.session.RowBounds;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * tjq
 */
@Service
public class IColumnServiceImpl extends BaseServiceImpl<Column> implements IColumnService {

    @Resource
    IColumnDao columnDao;

    @Override
    public void deleteColumnByModelId(Long modelId) {
        Table table = getTable();
        columnDao.deleteColumnByModelId(table.tableName(), modelId);
    }

    @Override
    public Page<Column> queryColumnByPage(Long modelId, RowBounds rowBounds) {
        Table table = getTable();
        return columnDao.queryColumnByPage(table.tableName(), modelId, rowBounds);
    }

    @Override
    public List<Column> queryColumnByModelId( Long modelId){
        Table table = getTable();
        return columnDao.queryColumnByModelId(table.tableName(), modelId);
    }

}
