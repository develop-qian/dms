package com.manage.dms.service.impl;

import com.github.pagehelper.Page;
import com.manage.dms.common.annotation.Table;
import com.manage.dms.domain.mapper.IModelDao;
import com.manage.dms.domain.model.Model;
import com.manage.dms.service.IModelService;
import org.apache.ibatis.session.RowBounds;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * tjq
 */
@Service
public class ModelServiceImpl extends BaseServiceImpl<Model> implements IModelService {

    @Resource
    IModelDao dao;

    @Override
    public Page<Model> queryModelByPage(Long projectId, RowBounds rowBounds) {
        Table table = getTable();
        return dao.queryModelByPage(table.tableName(), projectId, rowBounds);
    }

    @Override
    public List<Model> queryModelByProjectId(Long projectId) {
        Table table = getTable();
        return dao.queryModelByProjectId(table.tableName(), projectId);
    }
}
