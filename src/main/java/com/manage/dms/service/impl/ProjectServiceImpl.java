package com.manage.dms.service.impl;

import com.github.pagehelper.Page;
import com.manage.dms.common.annotation.Table;
import com.manage.dms.common.utils.IdGen;
import com.manage.dms.domain.mapper.IProjectDao;
import com.manage.dms.domain.model.Column;
import com.manage.dms.domain.model.Model;
import com.manage.dms.domain.model.Project;
import com.manage.dms.domain.model.UserProject;
import com.manage.dms.service.IColumnService;
import com.manage.dms.service.IModelService;
import com.manage.dms.service.IProjectService;
import com.manage.dms.service.IUserProjectService;
import org.apache.ibatis.session.RowBounds;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;

@Service
public class ProjectServiceImpl extends BaseServiceImpl<Project> implements IProjectService {

    @Resource
    private IProjectDao projectDao;
    @Resource
    private IUserProjectService userProjectService;
    @Resource
    private IModelService modelService;
    @Resource
    private IColumnService columnService;

    @Override
    public List<Project> queryProjectByProjectId(Long projectId) {
        Table table = getTable();
        return projectDao.queryProjectByProjectId(table.tableName(), projectId);
    }

    @Override
    @Transactional
    public void addUserProject(Long userId, Project info) {
        // 根据userId 和 projectId 查询userProject
        UserProject userProject = userProjectService.getInfoByUserIdAndProjectId(userId, info.getProjectId());
        // 判断是否存在
        if (userProject == null){
            userProject.setUserProjectId(IdGen.get().nextId());
            userProject.setProjectId(info.getProjectId());
            userProject.setUserId(userId);
            userProject.setCreateTime(System.currentTimeMillis());
            userProject.setCreateUser(userId);
            userProjectService.insert(userProject);
        }
        // 添加project
        insert(info);
    }

    @Override
    @Transactional
    public void deleteUserProject(Long userId, Long projectId) {
        // 根据userId 和 projectId 查询userProject
        UserProject userProject = userProjectService.getInfoByUserIdAndProjectId(userId, projectId);
        if (userProject != null){
            // 删除
            userProjectService.delete(userProject.getProjectId());
        }
        delete(projectId);
    }

    @Override
    public Page<Project> getUserProjects(Project info, RowBounds rowBounds) {
        Table table = getTable();
        return projectDao.getUserProjects(table.tableName(), info, rowBounds);
    }

    @Override
    public String generateSql(Long userId,Long projectId) {
        //查询项目信息
        UserProject userProject = userProjectService.getInfoByUserIdAndProjectId(userId, projectId);
        //查询实体表
        List<Model> models = modelService.queryModelByProjectId(projectId);
        for(Model model:models){
            //查询字段信息
            List<Column>  columns = columnService.queryColumnByModelId(model.getModelId());
        }

        return null;
    }
}
