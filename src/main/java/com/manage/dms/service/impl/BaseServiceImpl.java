package com.manage.dms.service.impl;

import com.alibaba.fastjson.JSON;
import com.manage.dms.common.annotation.Table;
import com.manage.dms.common.exceptions.MyException;
import com.manage.dms.common.utils.JsonUtils;
import com.manage.dms.common.utils.StringUtils;
import com.manage.dms.domain.mapper.IBaseDao;
import com.manage.dms.domain.model.BaseEntity;
import com.manage.dms.service.IBaseService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Resource;
import java.lang.reflect.ParameterizedType;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by idiot on 2018/4/29.
 * 该通用的方法仅仅支持  与数据库表相同的model
 * 对于vo等拓展类暂时不支持
 */
public abstract class BaseServiceImpl<T extends BaseEntity> implements IBaseService<T> {
    private static Logger logger = LoggerFactory.getLogger(BaseServiceImpl.class);

    @Resource
    private IBaseDao baseDao;

    private Class<T> type;

    public BaseServiceImpl(){
        // 参数化类型
        ParameterizedType parameterizedType = (ParameterizedType) this.getClass().getGenericSuperclass();
        // getActualTypeArguments() 返回表示此类型实际类型参数的 Type 对象的数组
        this.type = (Class<T>) parameterizedType.getActualTypeArguments()[0];
    }

    @Override
    public void insert(T info){
        if (info.getCreateTime() == null)
            info.setCreateTime(System.currentTimeMillis());
        // 实体信息转换成map
        Map paramMap = JSON.parseObject(JSON.toJSONString(info), Map.class);
        // 字段map
        Map titleMap = new HashMap();
        paramMap.forEach((key, value) -> titleMap.put(key, StringUtils.camelCaseToUnderLineCase(key.toString())));
        // 获取表名
        Table table = getTable();
        // 获取sql返回受影响的行数  小于1 表示操作失败
        int res = baseDao.insert(table.tableName(), titleMap, paramMap);
        if (res < 1){
            throw new MyException("添加信息失败");
        }
    }


    @Override
    public void update(T info) {
        info.setUpdateTime(System.currentTimeMillis());

        // 获取表名
        Table table = getTable();
        // 获取主键字段
        String primaryKey = table.primaryKey();
        // 实体信息转换成map
        Map paramMap = JSON.parseObject(JSON.toJSONString(info), Map.class);
        // 字段map
        Map columnsMap = new HashMap();
        paramMap.forEach((key, value) -> {
            // 去除主键字段和值
            String currentKey = StringUtils.camelCaseToUnderLineCase(key.toString());
            if (!StringUtils.camelCaseToUnderLineCase(primaryKey).equals(currentKey)){
                columnsMap.put(currentKey, value);
            }
        });

        // 获取主键值
        Object primaryKeyValue = paramMap.get(StringUtils.underLineCaseToCamelCase(primaryKey));
        // 获取sql返回受影响的行数  小于1 表示操作失败
        int res = baseDao.update(table.tableName(), primaryKey, primaryKeyValue, columnsMap);
        if (res < 1){
            throw new MyException("修改信息失败");
        }
    }

    @Override
    public void delete(Long id) {
        // 获取表名
        Table table = getTable();
        // 获取主键字段
        String primaryKey = table.primaryKey();
        // 获取sql返回受影响的行数  小于1 表示操作失败
        int res = baseDao.deleteById(table.tableName(), primaryKey, id);
        if (res < 1){
            throw new MyException("删除信息失败");
        }
    }

    @Override
    public T getInfoById(Long id){
        // 获取表名
        Table table = getTable();
        // 获取主键字段
        String primaryKey = table.primaryKey();
        /**
         * 获取返回值
         * 值得注意的是 该处返回的map是数据库中的字段
         * 并非经过mybatis插件 因此需要自己 处理
         * 下划线转驼峰命名
         */
        Map map = baseDao.getInfoById(table.tableName(), primaryKey, id);
        return (T) JsonUtils.mapToObject(map, this.type);
    }


    protected Table getTable() {
        // 获取表名
        Table table = this.type.getAnnotation(Table.class);
        if (table == null){
            throw new MyException("无效的注解@Table");
        }
        return table;
    }
}
