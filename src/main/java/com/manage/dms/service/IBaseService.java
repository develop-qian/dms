package com.manage.dms.service;

import com.manage.dms.common.annotation.Table;
import com.manage.dms.domain.model.BaseEntity;

/**
 * Created by idiot on 2018/4/29.
 */
public interface IBaseService<T extends BaseEntity> {

    /**
     * 新增
     * @param info
     * @return
     */
    void insert(T info);

    /**
     * 修改
     * @param info
     * @return
     */
    void update(T info);

    /**
     * 删除
     * @param id
     * @return
     */
    void delete(Long id);

    /**
     * 根据id获取信息
     * @param id
     * @return
     */
    T getInfoById(Long id);

}
