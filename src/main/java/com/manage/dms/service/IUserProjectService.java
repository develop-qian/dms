package com.manage.dms.service;

import com.github.pagehelper.Page;
import com.manage.dms.domain.model.UserProject;
import org.apache.ibatis.session.RowBounds;

import java.util.List;

public interface IUserProjectService extends IBaseService<UserProject> {

    //分页查询用户工程表
    Page<UserProject> queryUserProjectByPage(Long userId, RowBounds rowBounds);

    // 查询userProject
    UserProject getInfoByUserIdAndProjectId(Long userId, Long projectId);
}
