package com.manage.dms.service;

import com.manage.dms.domain.model.User;

/**
 * Created by idiot on 2018/4/29.
 */
public interface IUserService extends IBaseService<User> {

    // 根据用户名称密码获取用户信息
    User getUserByUserNameAndPassword(String userName, String password);
}
