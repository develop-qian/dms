
package com.manage.dms;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 * 启动类
 */
@SpringBootApplication
@EnableTransactionManagement
public class App {
    private static Logger logger = LoggerFactory.getLogger(App.class);
    public static void main( String[] args ) {
        logger.info( "【dms系统】启动!" );
        SpringApplication.run(App.class, args);
    }


    @Bean
    public Object transactionManagement(PlatformTransactionManager platformTransactionManager){
        logger.info("【事务管理器】>>>>>>>>>>" + platformTransactionManager.getClass().getName());
        return new Object();
    }
}
