package com.manage.dms.common.exceptions;

/**
 * @author idiot
 * @version 1.0.0
 * @date 16/5/2 上午10:50.
 */
public class MyException extends AbstractException {

    public MyException(String message) {
        super(message);
    }

}
