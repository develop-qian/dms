package com.manage.dms.common.exceptions;


import com.manage.dms.common.utils.Constants;
import com.manage.dms.domain.model.response.ResponseEntity;
import org.apache.shiro.ShiroException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;

/**
 * @description 全局异常处理类
 */
@ControllerAdvice
public class GlobalExceptionHandler {
    private static Logger logger = LoggerFactory.getLogger(GlobalExceptionHandler.class);

    @ExceptionHandler(value = Exception.class)
    @ResponseBody
    public Object defaultErrorHandler(HttpServletRequest req, Exception e) throws Exception {
        ResponseEntity responseEntity;
        if(e instanceof ShiroException){
            responseEntity = new ResponseEntity(Constants.ResponseCode.INVALID_RESULT, "您可能没有访问权限, 请联系管理员");
        }else{
            responseEntity = new ResponseEntity(Constants.ResponseCode.OTHER_EXCEPTION, e.getMessage());
        }
        logger.error("错误信息=====>" + e.getMessage());
        e.printStackTrace();
        return responseEntity;
    }

    /**
     * @description 自定义异常处理
     * @param req
     * @param e
     * @return
     * @throws Exception
     */
    @ExceptionHandler(value = MyException.class)
    @ResponseBody
    public Object exceptionHandler(HttpServletRequest req, MyException e) throws Exception {
        logger.error("自定义异常处理......错误信息: " + e.getMessage());
        return new ResponseEntity(Constants.ResponseCode.MY_EXCEPTION, e.getMessage());
    }

}

