package com.manage.dms.common.interceptor;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

/**
 * Created by idiot on 2018/5/7.
 * 自定义拦截器配置
 */
@Configuration
public class WebAppInterceptorConfig extends WebMvcConfigurerAdapter {

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        //添加拦截器  生效
        registry.addInterceptor(new NotNullParamInterceptor()).addPathPatterns("/api/**");
        registry.addInterceptor(new TokenInterceptor()).addPathPatterns("/api/**");
        registry.addInterceptor(new CheckFixedParamValueInterceptor()).addPathPatterns("/api/**");
        registry.addInterceptor(new CheckMinMaxParamValueInterceptor()).addPathPatterns("/api/**");
        super.addInterceptors(registry);
    }
}
