package com.manage.dms.common.interceptor;

import com.alibaba.fastjson.JSON;
import com.manage.dms.common.annotation.RequireToken;
import com.manage.dms.common.exceptions.MyException;
import com.manage.dms.common.utils.Constants;
import com.manage.dms.common.utils.TokenUtil;
import com.manage.dms.domain.model.response.ResponseEntity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.StringUtils;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;


/**
 * Created by idiot on 2018/2/5.
 */
public class TokenInterceptor implements HandlerInterceptor {
    private static Logger logger = LoggerFactory.getLogger(TokenInterceptor.class);

    @Override
    public boolean preHandle(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Object o) throws IOException {
        // 获取token的注解
        HandlerMethod handlerMethod = (HandlerMethod) o;
        RequireToken requireToken = handlerMethod.getMethodAnnotation(RequireToken.class);
        // 如果不存在RequireToken注解  返回true
        if (requireToken == null || requireToken.value() == false){
            return true;
        }
        //设置响应头
        httpServletResponse.setHeader("Content-Type","application/json;charset=UTF-8");
        // 获取token的参数值
        String tokenValue = httpServletRequest.getHeader(Constants.Regular.TOKEN);
        if (StringUtils.isEmpty(tokenValue)){
            // 输出错误信息
            logger.error("token拦截.............token不能为空");
            httpServletResponse.getWriter().write(JSON.toJSONString(new ResponseEntity(Constants.ResponseCode.NULL_PARAM, "token为空")));
            return false;
        }
        // 解密token
        String decodeToken = null;
        try {
            decodeToken = TokenUtil.decodeToken(tokenValue.getBytes());
        } catch (Exception e) {
            logger.error("token拦截.........【异常】........token解密失败");
            // e.printStackTrace();
            throw new MyException("token解密失败");
        }
        String[] message = decodeToken.split(Constants.Regular.HASHTAG);
        if (message == null || message.length != 2){
            // 输出错误信息
            logger.error("token拦截.............token解密失败");
            throw new MyException("token解密失败");
        }
        return true;
    }

    @Override
    public void postHandle(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Object o, ModelAndView modelAndView) throws Exception {

    }

    @Override
    public void afterCompletion(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Object o, Exception e) throws Exception {

    }
}
