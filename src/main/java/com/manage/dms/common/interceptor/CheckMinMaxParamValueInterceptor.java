package com.manage.dms.common.interceptor;

import com.alibaba.fastjson.JSON;
import com.manage.dms.common.annotation.CheckMinMaxParamValue;
import com.manage.dms.common.utils.Constants;
import com.manage.dms.domain.model.response.ResponseEntity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.MethodParameter;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Created by idiot on 2017/6/27.
 * @description   拦截 检测参数值 是否正常   最大最小值
 */
public class CheckMinMaxParamValueInterceptor implements HandlerInterceptor {
    private static Logger logger = LoggerFactory.getLogger(CheckMinMaxParamValueInterceptor.class);

    @Override
    public boolean preHandle(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Object o) throws Exception {
        // 获取方法中的参数  遍历
        HandlerMethod handlerMethod = (HandlerMethod) o;
        MethodParameter[] methodParameters = handlerMethod.getMethodParameters();
        boolean result = true;
        for(MethodParameter parameter : methodParameters){
            //判断当前参数  是否含有指定注解
            if(parameter.hasParameterAnnotation(CheckMinMaxParamValue.class)){
                //获取当前方法中的参数   拿到指定注解
                CheckMinMaxParamValue checkMinMaxParamValue = parameter.getParameterAnnotation(CheckMinMaxParamValue.class);

                //获取注解中的 属性  最小值 和 最大值
                int min = checkMinMaxParamValue.min();
                int max = checkMinMaxParamValue.max();
                String paramName = checkMinMaxParamValue.name();

                //从request对象中获取参数的值   然后与指定值作比较  不符合就返回错误信息
                String paramValue = httpServletRequest.getParameter(paramName);

                //如果当前参数的值  不在最小值和最大值之间  返回错误信息
                if(!(Integer.parseInt(paramValue) >= min && Integer.parseInt(paramValue) <= max)){
                    //设置响应头
                    httpServletResponse.setHeader("Content-Type","application/json;charset=UTF-8");
                    logger.error("拦截检测参数值错误........无效的参数：{"+paramName+":"+ paramValue+"}，允许范围：("+min+", "+max+")");
                    result = false;
                    //输出错误信息
                    httpServletResponse.getWriter().write(JSON.toJSONString(new ResponseEntity(Constants.ResponseCode.PARAM_OUTSIDE, String.format("参数%s值超出允许范围", paramName))));
                    break;
                }
            }
        }
        return result;
    }

    @Override
    public void postHandle(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Object o, ModelAndView modelAndView) throws Exception {

    }

    @Override
    public void afterCompletion(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Object o, Exception e) throws Exception {

    }
}
