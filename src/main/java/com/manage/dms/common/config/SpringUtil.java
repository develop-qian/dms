package com.manage.dms.common.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.annotation.Configuration;

/**
 * Created by idiot on 2017/1/4.
 */
@Configuration
public class SpringUtil implements ApplicationContextAware {
    private static Logger logger = LoggerFactory.getLogger(SpringUtil.class);

    private static ApplicationContext context;

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) {
        logger.info("spring 上下文=====>");
        this.context = applicationContext;
    }

    public static Object getBean(String name) {
        return context.getBean(name);
    }

    public static Object getBean(String name,Class<?> classz) {
        return context.getBean(name,classz);
    }

    public static ApplicationContext getContext() {
        return context;
    }
}
