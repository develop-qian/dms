package com.manage.dms.common.annotation;

import java.lang.annotation.*;

/**
 * Created by idiot on 2017/5/8.
 */
@Target(value = ElementType.METHOD)
@Retention(value = RetentionPolicy.RUNTIME)
@Documented
public @interface RecordLog {
    // 记录log的内容 内容需要格式化
    String value() default "";
    // 待写入日志的参数
    String[] params() default "";
}
