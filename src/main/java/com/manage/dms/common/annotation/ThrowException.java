package com.manage.dms.common.annotation;

import java.lang.annotation.*;

/**
 * Created by idiot on 2017/6/27.
 * @description 指定抛出异常
 */
@Target(value = ElementType.METHOD)
@Retention(value = RetentionPolicy.RUNTIME)
@Documented
public @interface ThrowException {
    //指定抛出的异常
    Class value() default Exception.class;
}
