package com.manage.dms.common.annotation;

import java.lang.annotation.*;

/**
 * Created by idiot on 2017/6/27.
 * @description 表名
 */
@Target(value = ElementType.TYPE)
@Retention(value = RetentionPolicy.RUNTIME)
@Documented
public @interface Table {
    // 表名
    String tableName() default "";
    // 主键名
    String primaryKey() default "";
}
