package com.manage.dms.common.annotation;

import java.lang.annotation.*;

/**
 * Created by idiot on 2017/6/27.
 * @description 非空参数校验
 */
@Target(value = ElementType.PARAMETER)
@Retention(value = RetentionPolicy.RUNTIME)
@Documented
public @interface CheckMinMaxParamValue {
    //参数名
    String name() default "";
    //最小值
    int min() default Integer.MIN_VALUE;
    //最大值
    int max() default Integer.MAX_VALUE;
}
