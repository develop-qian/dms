package com.manage.dms.common.utils;

import com.manage.dms.common.exceptions.MyException;
import org.apache.shiro.codec.Base64;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.crypto.SecretKey;

/**
 * Created by idiot on 2017/1/3.
 * @description 生成token  以及对token的加密   解密
 */
public class TokenUtil {
    private static Logger logger = LoggerFactory.getLogger(TokenUtil.class);

    public static String encodeToken(String message){
        //先使用encryAES加密  之后用Base64加密
        byte[] token = null;
        try {
            token = Base64.encode(EncrypAES.Encrytor(message, null));
        }catch (Exception e){
            logger.error("token生成失败");
            throw new MyException("token生成失败");
        }
        return new String(token);
    }

    public static String decodeToken(byte[] token) throws Exception {
        //获取加密后的数组  先使用Base64解密  之后使用encryAES解密
        byte[] message = null;
        message = EncrypAES.Decryptor(Base64.decode(token), null);
        return new String(message);
    }


    public static String encodeTokenByKey(String message, SecretKey secretKey){
        //先使用encryAES加密  之后用Base64加密
        byte[] token = null;
        try {
            token = Base64.encode(EncrypAES.Encrytor(message, secretKey));
        }catch (Exception e){
            logger.error("token生成失败");
        }
        return new String(token);
    }

    public static String decodeTokenByKey(byte[] token, SecretKey secretKey) throws Exception {
        //获取加密后的数组  先使用Base64解密  之后使用encryAES解密
        byte[] message = null;
        message = EncrypAES.Decryptor(Base64.decode(token), secretKey);
        return new String(message);
    }

    /**
     * @description 根据token的创建时间  有效时间  和当前时间  判断改token是否失效
     * @param tokenTime
     * @param TokenVlidTime
     * @return  true 未失效   false 失效
     */
    public static boolean validToken(long tokenTime, long TokenVlidTime){
        boolean valid = false;
        long currentTime = System.currentTimeMillis();
        if(tokenTime + TokenVlidTime > currentTime){
            valid = true;
        }
        return valid;
    }

    public static void main(String[] args) throws Exception {
        String token  = TokenUtil.encodeToken("admin;"+ System.currentTimeMillis());
        logger.info(token);
        String message = TokenUtil.decodeToken(token.getBytes());
        logger.info(message);

        token  = TokenUtil.encodeToken("admin;"+ System.currentTimeMillis());
        logger.info(token);
        message = TokenUtil.decodeToken(token.getBytes());
        logger.info(message);
    }
}
