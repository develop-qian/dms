package com.manage.dms.common.utils;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.serializer.PropertyFilter;
import com.alibaba.fastjson.serializer.ValueFilter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;


/**
 * Created by idiot on 2017/7/20.
 */
public class JsonUtils {
    private static Logger logger = LoggerFactory.getLogger(JsonUtils.class);

    /**
     * @description  json 过滤
     * @return
     */
    public static ValueFilter initValueFilter(final String ... params){
        //把long类型的转换为字符串
        ValueFilter valueFilter = new ValueFilter() {
            @Override
            public Object process(Object o, String name, Object value) {
                if(value instanceof Long){
                    return String.valueOf(value);
                }
                if(value instanceof Date){
                    return ((Date) value).getTime();
                }
                for (String param : params){
                    if(name.equals(param) && value == null){
                        return "";
                    }
                }
                return value;
            }
        };
        return valueFilter;
    }

    /**
     * @description  json 过滤 带排除属性
     * @return
     */
    private static ValueFilter initValueFilter(final String[] excludes, final String... params) {
        //把long类型的转换为字符串
        ValueFilter valueFilter = new ValueFilter() {
            @Override
            public Object process(Object o, String name, Object value) {
                // 先排除部分属性
                for (String exclude : excludes){
                    if (name.equals(exclude)){
                        return null;
                    }
                }
                // 未排除之后的属性  继续过滤
                if(value instanceof Long){
                    return String.valueOf(value);
                }
                if(value instanceof Date){
                    return ((Date) value).getTime();
                }
                for (String param : params){
                    if(name.equals(param) && value == null){
                        return "";
                    }
                }
                return value;
            }
        };
        return valueFilter;
    }


    public static PropertyFilter initPropertyFilter(final String... ids){
        PropertyFilter propertyFilter = new PropertyFilter() {
            @Override
            public boolean apply(Object o, String name, Object value) {
                for (String id : ids){
                    if(name.equals(id)){
                        return false;
                    }
                }
                return true;
            }
        };
        return propertyFilter;
    }

    /**
     * @descripton   list -->  json 过滤之后的 jsonArray
     * @param list
     * @param params   设置过滤的属性  当null时返回空字符串
     * @return
     */
    public static JSONArray parseArray(List list, String ... params){
        //json过滤
        ValueFilter valueFilter = JsonUtils.initValueFilter(params);
        JSONArray object = JSON.parseArray(JSON.toJSONString(list, valueFilter));
        return object;
    }

    /**
     * @descripton   list -->  json 过滤之后的 jsonArray
     * @param list
     * @param excludes 排除的属性
     * @param params   设置过滤的属性  当null时返回空字符串
     * @return
     */
    public static JSONArray parseArrayWithExcludes(List list, String[] excludes, String ... params){
        //json过滤
        ValueFilter valueFilter = JsonUtils.initValueFilter(excludes, params);
        JSONArray object = JSON.parseArray(JSON.toJSONString(list, valueFilter));
        return object;
    }


    /**
     * @descripton   Object -->  json 过滤之后的 jsonObject
     * @param obj
     * @return
     */
    public static JSONObject parseObject(Object obj){
        //json过滤
        ValueFilter valueFilter = JsonUtils.initValueFilter();
        JSONObject object = JSON.parseObject(JSON.toJSONString(obj, valueFilter));
        return object;
    }


    /**
     * key数组  和 value数组  转换成json
     * @param keys
     * @param values
     * @return
     */
    public static JSONObject arraysToJson(String[] keys, Object[] values){
        HashMap<String, Object> map = new HashMap<>();
        // 遍历数组  组装map
        for (int i=0; i<keys.length; i++){
            map.put(keys[i], values[i] == null ? "" : values[i]);
        }
        JSONObject jsonObject = JsonUtils.parseObject(JSON.parseObject(JSON.toJSONString(map)));
        return jsonObject;
    }

    /**
     * 获取jsonObject中 key对应的值
     * @param jsonObject
     * @param params
     * @return
     */
    public static Object[] getArgs(JSONObject jsonObject, String[] params) {
        List args = new ArrayList();
        for (String param : params){
            args.add(jsonObject.get(param));
        }
        return args.toArray();
    }


    /**
     * map转 实体
     * @param map
     * @param clazz
     * @return
     */
    public static Object mapToObject(Map map, Class clazz){
        Map propertiesMap = new HashMap();
        // map 返回值有可能为空
        if (map == null){
            return null;
        }
        map.forEach((key, value) -> {
            // 下划线转驼峰
            String currentKey = StringUtils.underLineCaseToCamelCase(key.toString());
            propertiesMap.put(currentKey, value);
        });
        return JSON.parseObject(JSON.toJSONString(propertiesMap), clazz);
    }
}
