package com.manage.dms.common.utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by idiot on 2017/4/10.
 * @description  获取系统相关信息
 */
public class SystemUtils {
    private static Logger logger = LoggerFactory.getLogger(SystemUtils.class);

    private static final String OS = System.getProperty("os.name").toLowerCase();

    private static final String WINDOWS = "windows";
    private static final String JAVA8 = "1.8";

    /**
     * @description判断当前系统是否是windows
     * @return
     */
    public static boolean isWindows(){
        return OS.indexOf(WINDOWS) >= 0;
    }

    /**
     * 获取当前系统的jdk版本
     * @return
     */
    public static String getJavaVersion(){
        String javaVersion = System.getProperty("java.version");
        return javaVersion.substring(0, 3);
    }

    /**
     * 判断当前系统是否是java8
     * @return
     */
    public static boolean isJava8(){
        if (getJavaVersion().equals(JAVA8))
            return true;
        return false;
    }
}
