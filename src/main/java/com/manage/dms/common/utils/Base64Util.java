package com.manage.dms.common.utils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by idiot on 2018/4/10.
 */
public class Base64Util {


    /**
     * base64 encode
     * @param message
     * @param salt
     * @return
     */
    public static String encodeBase64(String message, String salt){
        // 取出源字符串中 第一个数字
        int firstNumber = getFirstNumber(message);
        // 设置默认值
        if (org.springframework.util.StringUtils.isEmpty(salt))
            salt = new String(Constants.Regular.SIGN_NAME);
        StringBuffer temp = new StringBuffer();
        temp.append(message.substring(0, firstNumber)).append(salt).append(message.substring(firstNumber, message.length()));
        return java.util.Base64.getEncoder().encodeToString(temp.toString().getBytes());
    }

    /**
     * base64 decode
     * @param message
     * @param salt
     * @return
     */
    public static String decodeBase64(String message, String salt){
        // 设置默认值
        if (org.springframework.util.StringUtils.isEmpty(salt))
            salt = new String(Constants.Regular.SIGN_NAME);
        String temp = new String(java.util.Base64.getDecoder().decode(message));
        // 取出源字符串中 第一个数字
        int firstNumber = getFirstNumber(temp);
        String res = temp.substring(0, firstNumber) + temp.substring(firstNumber + salt.length(), temp.length());
        return res;
    }

    /**
     * 获取第一个数字
     * @param param1
     * @return
     */
    private static int getFirstNumber(String param1){
        // 取出源字符串中 第一个数字
        int firstNumber = 0;
        String reg = "\\d{1}";
        Pattern p = Pattern.compile(reg);
        Matcher m = p.matcher(param1); // 获取 matcher 对象
        if (m.find()){
            firstNumber = Integer.valueOf(m.group());
        }
        return firstNumber;
    }

}
