package com.manage.dms.common.utils;

/**
 * Created by idiot on 2016/12/15.
 *
 * @description 常量
 */
public class Constants {

    /**
     * 返回值 常量池   变量名大写  用_分割
     */
    public static class ResponseCode {
        // 成功
        public static final Integer SUCCESS = 0;
        // 参数为空
        public static final Integer NULL_PARAM = 1000;
        // 参数值不在允许范围
        public static final Integer PARAM_OUTSIDE = 1001;
        // 参数值无效
        public static final Integer INVALID_PARAM = 1002;
        // 无效的token
        public static final Integer INVALID_TOKEN = 1003;
        // 信息不存在
        public static final Integer INVALID_RESULT = 1004;
        // 自定义异常
        public static final Integer MY_EXCEPTION = 1005;
        // 其他异常
        public static final Integer OTHER_EXCEPTION = 1006;
    }

    /**
     * 其他常量
     */
    public static class Regular {
        // 数据库签名混淆值
        public static final String SIGN_NAME = "DMS";
        // token
        public static final String TOKEN = "Token";
        // #
        public static final String HASHTAG = "#";
    }

}
