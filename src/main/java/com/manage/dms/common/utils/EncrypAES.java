package com.manage.dms.common.utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.crypto.*;
import java.security.InvalidKeyException;
import java.security.Security;

/**
 * 对称加密
 * Created by idiot on 2016/12/12.
 */
public class EncrypAES {
    private static Logger logger = LoggerFactory.getLogger(EncrypAES.class);

    //KeyGenerator 提供对称密钥生成器的功能，支持各种算法
    private static KeyGenerator keygen;
    //SecretKey 负责保存对称密钥
    private static SecretKey deskey;
    //Cipher负责完成加密或解密工作
    private static Cipher c;
    //该字节数组负责保存加密的结果
    private static byte[] cipherByte;

    static {
        try {
            Security.addProvider(new com.sun.crypto.provider.SunJCE());
            //实例化支持DES算法的密钥生成器(算法名称命名需按规定，否则抛出异常)
            keygen = KeyGenerator.getInstance("AES");
            //生成密钥
            deskey = keygen.generateKey();
            //生成Cipher对象,指定其支持的DES算法
            c = Cipher.getInstance("AES");
        }catch (Exception e){
            logger.error("初始化【EncrypAES】失败");
        }
    }

    /**
     * 对字符串加密
     */
    public static byte[] Encrytor(String str, SecretKey secretKey) throws InvalidKeyException, IllegalBlockSizeException, BadPaddingException {
        // 根据密钥，对Cipher对象进行初始化，ENCRYPT_MODE表示加密模式
        if (secretKey != null)
            c.init(Cipher.ENCRYPT_MODE, secretKey);
        else
            c.init(Cipher.ENCRYPT_MODE, deskey);
        byte[] src = str.getBytes();
        // 加密，结果保存进cipherByte  
        cipherByte = c.doFinal(src);
        return cipherByte;
    }

    /**
     * 对字符串解密
     */
    public static byte[] Decryptor(byte[] buff, SecretKey secretKey) throws InvalidKeyException,
            IllegalBlockSizeException, BadPaddingException {
        // 根据密钥，对Cipher对象进行初始化，DECRYPT_MODE表示加密模式
        if (secretKey != null)
            c.init(Cipher.DECRYPT_MODE, secretKey);
        else
            c.init(Cipher.DECRYPT_MODE, deskey);
        cipherByte = c.doFinal(buff);
        return cipherByte;
    }

    public static SecretKey getDeskey() {
        return deskey;
    }

}
