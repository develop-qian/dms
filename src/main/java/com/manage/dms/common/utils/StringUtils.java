package com.manage.dms.common.utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Random;
import java.util.UUID;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by idiot on 2017/1/16.
 */
public class StringUtils {
    private static Logger logger = LoggerFactory.getLogger(StringUtils.class);


    /**
     * @description 生成uuid
     * @return
     */
    public static String generateUUID(){
        return UUID.randomUUID().toString().replaceAll("-", "");
    }


    //随机数
    public static String randomValue(int seed){
        return String.valueOf(new Random().nextInt(seed));
    }


    /**
     * 获取指定字符串出现的次数
     *
     * @param srcText 源字符串
     * @param findText 要查找的字符串
     * @return
     */
    public static int appearNumber(String srcText, String findText) {
        int count = 0;
        Pattern p = Pattern.compile(findText);
        Matcher m = p.matcher(srcText);
        while (m.find()) {
            count++;
        }
        return count;
    }


    /**
     * 驼峰转下划线
     * @param param
     * @return
     */
    public static String camelCaseToUnderLineCase(String param){
        return param.replaceAll("[A-Z]" ,"_$0").toLowerCase();
    }


    /**
     * 下划线转驼峰
     * @param str
     * @return
     */
    public static String underLineCaseToCamelCase(String str){
        // 正则匹配表达式
        Pattern linePattern = Pattern.compile("_(\\w)");
        str = str.toLowerCase();
        Matcher matcher = linePattern.matcher(str);
        StringBuffer sb = new StringBuffer();
        while(matcher.find()){
            matcher.appendReplacement(sb, matcher.group(1).toUpperCase());
        }
        matcher.appendTail(sb);
        return sb.toString();
    }
}
