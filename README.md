# dms

#### 项目介绍
数据库管理系统

### api接口文档
[接口文档详情](http://the_eleven.gitee.io/dms/apidoc/)

#### 软件架构
软件架构说明

---
数据库设计1.0版 2018.5.4
![数据库设计1.0版](https://gitee.com/uploads/images/2018/0504/172859_efd6eaba_1153562.png "数据库设计1.0版.png")

* 关于sql文件，存储在数据库中，t_sql_file表，字段：file_name(文件名)，projectId(sql在工程下面)，content(内容)，file_size......
* sql在数据库中方便读取，以二进制数据存储
#### 安装教程

1. xxxx
2. xxxx
3. xxxx

#### 使用说明

1. xxxx
2. xxxx
3. xxxx

#### 参与贡献

1. Fork 本项目
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request


#### 码云特技

1. 使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2. 码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3. 你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4. [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5. 码云官方提供的使用手册 [http://git.mydoc.io/](http://git.mydoc.io/)
6. 码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)