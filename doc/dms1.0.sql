/*
*** 数据库sql
*/
-- 用户信息表
DROP TABLE IF EXISTS t_user;
CREATE TABLE t_user (
	user_id bigint(20) NOT NULL COMMENT '用户信息id',
	user_name varchar(255) NOT NULL COMMENT '用户名',
	password varchar(255) NOT NULL COMMENT '用户密码',
	kind  int(11) DEFAULT '0' COMMENT '类型{0: 用户, 1: 管理员}',
	create_time bigint(20) DEFAULT NULL COMMENT '创建时间',
	update_time bigint(20) DEFAULT NULL COMMENT '修改时间',
	create_user bigint(20) DEFAULT NULL COMMENT '创建用户',
	update_user bigint(20) DEFAULT NULL COMMENT '修改用户',
	status int(11) DEFAULT '1' COMMENT '状态 {1: 有效, 0: 无效}',
	remark varchar(255) DEFAULT NULL COMMENT '备注',
	PRIMARY KEY (user_id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


-- 项目信息表
DROP TABLE IF EXISTS t_project;
CREATE TABLE t_project(
	project_id bigint(20) NOT NULL COMMENT '项目id',
	project_name varchar(255) NOT NULL COMMENT '项目名称',
	create_time bigint(20) DEFAULT NULL COMMENT '创建时间',
	update_time bigint(20) DEFAULT NULL COMMENT '修改时间',
	create_user bigint(20) DEFAULT NULL COMMENT '创建用户',
	update_user bigint(20) DEFAULT NULL COMMENT '修改用户',
	status int(11) DEFAULT '1' COMMENT '状态 {1: 有效, 0: 无效}',
	remark varchar(255) DEFAULT NULL COMMENT '备注',
	PRIMARY KEY (project_id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


-- 用户项目关联信息表
DROP TABLE IF EXISTS t_user_project;
CREATE TABLE t_user_project(
	user_project_id bigint(20) NOT NULL COMMENT '用户项目关联id',
	user_id bigint(20) NOT NULL COMMENT '用户id',
	project_id bigint(20) NOT NULL COMMENT '项目id',
	create_time bigint(20) DEFAULT NULL COMMENT '创建时间',
	update_time bigint(20) DEFAULT NULL COMMENT '修改时间',
	create_user bigint(20) DEFAULT NULL COMMENT '创建用户',
	update_user bigint(20) DEFAULT NULL COMMENT '修改用户',
	status int(11) DEFAULT '1' COMMENT '状态 {1: 有效, 0: 无效}',
	remark varchar(255) DEFAULT NULL COMMENT '备注',
	PRIMARY KEY (user_project_id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


-- 实体信息表
DROP TABLE IF EXISTS t_model;
CREATE TABLE t_model(
	model_id bigint(20) NOT NULL COMMENT '实体信息id',
	project_id bigint(20) NOT NULL COMMENT '项目id',
	model_name varchar(255) NOT NULL COMMENT '名称',
	create_time bigint(20) DEFAULT NULL COMMENT '创建时间',
	update_time bigint(20) DEFAULT NULL COMMENT '修改时间',
	create_user bigint(20) DEFAULT NULL COMMENT '创建用户',
	update_user bigint(20) DEFAULT NULL COMMENT '修改用户',
	status int(11) DEFAULT '1' COMMENT '状态 {1: 有效, 0: 无效}',
	remark varchar(255) DEFAULT NULL COMMENT '备注',
	PRIMARY KEY (model_id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


-- 字段信息表
DROP TABLE IF EXISTS t_column;
CREATE TABLE t_column(
	column_id bigint(20) NOT NULL COMMENT '字段id',
	model_id bigint(20) NOT NULL COMMENT '实体id',
	column_name varchar(255) NOT NULL COMMENT '字段名称',
	column_type varchar(50) NOT NULL COMMENT '字段类型',
	column_length int(11) DEFAULT '0' COMMENT '字段长度',
	null_flag int(11) DEFAULT NULL COMMENT '是否为空',
	default_value varchar(20) DEFAULT NULL COMMENT '默认值',
	primary_key_flag int(11) DEFAULT '0' COMMENT '是否是主键{1: 是, 0: 否}',
	decimal_place int(11) DEFAULT '0' COMMENT '保留小数位',
	foreign_key_flag int(11) DEFAULT '0' COMMENT '是否是外键{1: 是, 0: 否}',
	foreign_model_id bigint(20) DEFAULT NULL COMMENT '外键关联的实体id',
	foregin_column_id bigint(20) DEFAULT NULL COMMENT '外键关联的字段id',
	create_time bigint(20) DEFAULT NULL COMMENT '创建时间',
	update_time bigint(20) DEFAULT NULL COMMENT '修改时间',
	create_user bigint(20) DEFAULT NULL COMMENT '创建用户',
	update_user bigint(20) DEFAULT NULL COMMENT '修改用户',
	status int(11) DEFAULT '1' COMMENT '状态 {1: 有效, 0: 无效}',
	remark varchar(255) DEFAULT NULL COMMENT '备注',
	PRIMARY KEY (column_id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



