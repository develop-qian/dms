define({ "api": [
  {
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "optional": false,
            "field": "varname1",
            "description": "<p>No type.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "varname2",
            "description": "<p>With type.</p>"
          }
        ]
      }
    },
    "type": "",
    "url": "",
    "version": "0.0.0",
    "filename": "./apidoc/main.js",
    "group": "C__Users_idiot_Desktop_dms_apidoc_main_js",
    "groupTitle": "C__Users_idiot_Desktop_dms_apidoc_main_js",
    "name": ""
  },
  {
    "type": "post",
    "url": "/api/column/deleteByColumnId.json",
    "title": "删除字段接口",
    "version": "1.0.0",
    "group": "column",
    "name": "delete",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "token",
            "description": "<p>用户token</p>"
          }
        ]
      }
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Long",
            "optional": false,
            "field": "columnId",
            "description": "<p>字段ID</p>"
          }
        ]
      }
    },
    "filename": "./src/main/java/com/manage/dms/web/api/ColumnController.java",
    "groupTitle": "column",
    "sampleRequest": [
      {
        "url": "http://114.80.222.219:8088/dms/api/column/deleteByColumnId.json"
      }
    ],
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "code",
            "description": "<p>状态码</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "codeMsg",
            "description": "<p>状态码描述信息</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "data",
            "description": "<p>数据</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "参数异常请求返回示例:",
          "content": "HTTP/1.1 200 OK\n{\n  \"code\": 1000,\n  \"msg\": \"参数为空\"\n  \"data\": null\n}",
          "type": "json"
        }
      ]
    }
  },
  {
    "type": "post",
    "url": "/api/column/queryColumnByPage.json",
    "title": "根据modelId分页获取字段接口",
    "version": "1.0.0",
    "group": "column",
    "name": "queryColumnByPage",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "token",
            "description": "<p>用户token</p>"
          }
        ]
      }
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Long",
            "optional": false,
            "field": "modelId",
            "description": "<p>实体Id</p>"
          },
          {
            "group": "Parameter",
            "type": "Integer",
            "optional": false,
            "field": "pageNo",
            "description": "<p>页码</p>"
          },
          {
            "group": "Parameter",
            "type": "Integer",
            "optional": false,
            "field": "pageSize",
            "description": "<p>条数</p>"
          }
        ]
      }
    },
    "filename": "./src/main/java/com/manage/dms/web/api/ColumnController.java",
    "groupTitle": "column",
    "sampleRequest": [
      {
        "url": "http://114.80.222.219:8088/dms/api/column/queryColumnByPage.json"
      }
    ],
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "code",
            "description": "<p>状态码</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "codeMsg",
            "description": "<p>状态码描述信息</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "data",
            "description": "<p>数据</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "参数异常请求返回示例:",
          "content": "HTTP/1.1 200 OK\n{\n  \"code\": 1000,\n  \"msg\": \"参数为空\"\n  \"data\": null\n}",
          "type": "json"
        }
      ]
    }
  },
  {
    "type": "post",
    "url": "/api/column/update.json",
    "title": "编辑字段接口",
    "version": "1.0.0",
    "group": "column",
    "name": "update",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "token",
            "description": "<p>用户token</p>"
          }
        ]
      }
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": true,
            "field": "columnId",
            "description": "<p>字段ID 若有值就是修改 没有值就是新增</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "modelId",
            "description": "<p>实体id 必填</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "columnName",
            "description": "<p>字段名称</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "columnType",
            "description": "<p>字段类型  -- 可选值 varchar  int  bigint .....</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "columnLength",
            "description": "<p>字段长度</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "nullFlag",
            "description": "<p>是否为空</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "defaultValue",
            "description": "<p>默认值</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "primaryKeyFlag",
            "description": "<p>是否是主键</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "decimalPlace",
            "description": "<p>保留小数位</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "foreignKeyFlag",
            "description": "<p>是否是外键{1: 是, 0: 否}</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "foreignModelId",
            "description": "<p>外键关联的实体id</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "foreignColumnId",
            "description": "<p>外键关联的字段id</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "data.columnId",
            "description": "<p>字段id</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "code",
            "description": "<p>状态码</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "codeMsg",
            "description": "<p>状态码描述信息</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "data",
            "description": "<p>数据</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "参数异常请求返回示例:",
          "content": "HTTP/1.1 200 OK\n{\n  \"code\": 1000,\n  \"msg\": \"参数为空\"\n  \"data\": null\n}",
          "type": "json"
        }
      ]
    },
    "filename": "./src/main/java/com/manage/dms/web/api/ColumnController.java",
    "groupTitle": "column",
    "sampleRequest": [
      {
        "url": "http://114.80.222.219:8088/dms/api/column/update.json"
      }
    ]
  },
  {
    "type": "post",
    "url": "/api/model/deleteModel.json",
    "title": "删除model信息接口",
    "version": "1.0.0",
    "group": "model",
    "name": "deleteModel",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "Token",
            "description": "<p>用户token</p>"
          }
        ]
      }
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "modelId",
            "description": "<p>实体id</p>"
          }
        ]
      }
    },
    "filename": "./src/main/java/com/manage/dms/web/api/ModelController.java",
    "groupTitle": "model",
    "sampleRequest": [
      {
        "url": "http://114.80.222.219:8088/dms/api/model/deleteModel.json"
      }
    ],
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "code",
            "description": "<p>状态码</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "codeMsg",
            "description": "<p>状态码描述信息</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "data",
            "description": "<p>数据</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "参数异常请求返回示例:",
          "content": "HTTP/1.1 200 OK\n{\n  \"code\": 1000,\n  \"msg\": \"参数为空\"\n  \"data\": null\n}",
          "type": "json"
        }
      ]
    }
  },
  {
    "type": "post",
    "url": "/api/model/getModelByModelId.json",
    "title": "根据modelId获取model详情接口",
    "version": "1.0.0",
    "group": "model",
    "name": "getModelByModelId",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "Token",
            "description": "<p>用户token</p>"
          }
        ]
      }
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "modelId",
            "description": "<p>项目Id</p>"
          }
        ]
      }
    },
    "filename": "./src/main/java/com/manage/dms/web/api/ModelController.java",
    "groupTitle": "model",
    "sampleRequest": [
      {
        "url": "http://114.80.222.219:8088/dms/api/model/getModelByModelId.json"
      }
    ],
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "code",
            "description": "<p>状态码</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "codeMsg",
            "description": "<p>状态码描述信息</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "data",
            "description": "<p>数据</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "参数异常请求返回示例:",
          "content": "HTTP/1.1 200 OK\n{\n  \"code\": 1000,\n  \"msg\": \"参数为空\"\n  \"data\": null\n}",
          "type": "json"
        }
      ]
    }
  },
  {
    "type": "post",
    "url": "/api/model/queryModelByPage.json",
    "title": "根据projectId查询实体信息接口",
    "version": "1.0.0",
    "group": "model",
    "name": "queryModelByPage",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "Token",
            "description": "<p>用户token</p>"
          }
        ]
      }
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "projectId",
            "description": "<p>项目Id</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "pageNo",
            "description": "<p>页码</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "pageSize",
            "description": "<p>条数</p>"
          }
        ]
      }
    },
    "filename": "./src/main/java/com/manage/dms/web/api/ModelController.java",
    "groupTitle": "model",
    "sampleRequest": [
      {
        "url": "http://114.80.222.219:8088/dms/api/model/queryModelByPage.json"
      }
    ],
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "code",
            "description": "<p>状态码</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "codeMsg",
            "description": "<p>状态码描述信息</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "data",
            "description": "<p>数据</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "参数异常请求返回示例:",
          "content": "HTTP/1.1 200 OK\n{\n  \"code\": 1000,\n  \"msg\": \"参数为空\"\n  \"data\": null\n}",
          "type": "json"
        }
      ]
    }
  },
  {
    "type": "post",
    "url": "/api/model/updateModel.json",
    "title": "编辑model信息接口",
    "version": "1.0.0",
    "group": "model",
    "name": "updateModel",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "Token",
            "description": "<p>用户token</p>"
          }
        ]
      }
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": true,
            "field": "modelId",
            "description": "<p>实体ID 若有值就是修改 没有值就是新增</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "projectId",
            "description": "<p>工程id</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "modelName",
            "description": "<p>实体名称</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "data.modelId",
            "description": "<p>实体id</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "code",
            "description": "<p>状态码</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "codeMsg",
            "description": "<p>状态码描述信息</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "data",
            "description": "<p>数据</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "参数异常请求返回示例:",
          "content": "HTTP/1.1 200 OK\n{\n  \"code\": 1000,\n  \"msg\": \"参数为空\"\n  \"data\": null\n}",
          "type": "json"
        }
      ]
    },
    "filename": "./src/main/java/com/manage/dms/web/api/ModelController.java",
    "groupTitle": "model",
    "sampleRequest": [
      {
        "url": "http://114.80.222.219:8088/dms/api/model/updateModel.json"
      }
    ]
  },
  {
    "type": "post",
    "url": "/api/project/deleteById.json",
    "title": "删除工程接口",
    "version": "1.0.0",
    "group": "project",
    "name": "deleteById",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "Token",
            "description": "<p>用户token</p>"
          }
        ]
      }
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Long",
            "optional": false,
            "field": "data.projectId",
            "description": "<p>项目Id</p>"
          }
        ]
      }
    },
    "filename": "./src/main/java/com/manage/dms/web/api/ProjectController.java",
    "groupTitle": "project",
    "sampleRequest": [
      {
        "url": "http://114.80.222.219:8088/dms/api/project/deleteById.json"
      }
    ],
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "code",
            "description": "<p>状态码</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "codeMsg",
            "description": "<p>状态码描述信息</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "data",
            "description": "<p>数据</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "参数异常请求返回示例:",
          "content": "HTTP/1.1 200 OK\n{\n  \"code\": 1000,\n  \"msg\": \"参数为空\"\n  \"data\": null\n}",
          "type": "json"
        }
      ]
    }
  },
  {
    "type": "post",
    "url": "/api/project/getProjectByProjectId.json",
    "title": "根据工程id获取工程信息接口",
    "version": "1.0.0",
    "group": "project",
    "name": "getProjectByProjectId",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "Token",
            "description": "<p>用户token</p>"
          }
        ]
      }
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "projectId",
            "description": "<p>项目Id</p>"
          }
        ]
      }
    },
    "filename": "./src/main/java/com/manage/dms/web/api/ProjectController.java",
    "groupTitle": "project",
    "sampleRequest": [
      {
        "url": "http://114.80.222.219:8088/dms/api/project/getProjectByProjectId.json"
      }
    ],
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "code",
            "description": "<p>状态码</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "codeMsg",
            "description": "<p>状态码描述信息</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "data",
            "description": "<p>数据</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "参数异常请求返回示例:",
          "content": "HTTP/1.1 200 OK\n{\n  \"code\": 1000,\n  \"msg\": \"参数为空\"\n  \"data\": null\n}",
          "type": "json"
        }
      ]
    }
  },
  {
    "type": "post",
    "url": "/api/project/getUserProjects.json",
    "title": "获取用户的工程信息接口",
    "version": "1.0.0",
    "group": "project",
    "name": "getUserProjects",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "Token",
            "description": "<p>用户token</p>"
          }
        ]
      }
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "pageNo",
            "description": "<p>页码</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "pageSize",
            "description": "<p>条数</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "projectName",
            "description": "<p>项目名称 模糊搜索</p>"
          }
        ]
      }
    },
    "filename": "./src/main/java/com/manage/dms/web/api/ProjectController.java",
    "groupTitle": "project",
    "sampleRequest": [
      {
        "url": "http://114.80.222.219:8088/dms/api/project/getUserProjects.json"
      }
    ],
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "code",
            "description": "<p>状态码</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "codeMsg",
            "description": "<p>状态码描述信息</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "data",
            "description": "<p>数据</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "参数异常请求返回示例:",
          "content": "HTTP/1.1 200 OK\n{\n  \"code\": 1000,\n  \"msg\": \"参数为空\"\n  \"data\": null\n}",
          "type": "json"
        }
      ]
    }
  },
  {
    "type": "post",
    "url": "/api/project/generateSql.json",
    "title": "生成用户Sql接口",
    "version": "1.0.0",
    "group": "project",
    "name": "getUserProjects",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "Token",
            "description": "<p>用户token</p>"
          }
        ]
      }
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "projectId",
            "description": "<p>项目Id</p>"
          }
        ]
      }
    },
    "filename": "./src/main/java/com/manage/dms/web/api/ProjectController.java",
    "groupTitle": "project",
    "sampleRequest": [
      {
        "url": "http://114.80.222.219:8088/dms/api/project/generateSql.json"
      }
    ],
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "code",
            "description": "<p>状态码</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "codeMsg",
            "description": "<p>状态码描述信息</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "data",
            "description": "<p>数据</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "参数异常请求返回示例:",
          "content": "HTTP/1.1 200 OK\n{\n  \"code\": 1000,\n  \"msg\": \"参数为空\"\n  \"data\": null\n}",
          "type": "json"
        }
      ]
    }
  },
  {
    "type": "post",
    "url": "/api/project/updateProject.json",
    "title": "编辑工程接口",
    "version": "1.0.0",
    "group": "project",
    "name": "updateProject",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "Token",
            "description": "<p>用户token</p>"
          }
        ]
      }
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": true,
            "field": "projectId",
            "description": "<p>工程id 若有值就是修改 没有值就是新增</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "projectName",
            "description": "<p>工程名称</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "projectId",
            "description": "<p>工程id</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "code",
            "description": "<p>状态码</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "codeMsg",
            "description": "<p>状态码描述信息</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "data",
            "description": "<p>数据</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "参数异常请求返回示例:",
          "content": "HTTP/1.1 200 OK\n{\n  \"code\": 1000,\n  \"msg\": \"参数为空\"\n  \"data\": null\n}",
          "type": "json"
        }
      ]
    },
    "filename": "./src/main/java/com/manage/dms/web/api/ProjectController.java",
    "groupTitle": "project",
    "sampleRequest": [
      {
        "url": "http://114.80.222.219:8088/dms/api/project/updateProject.json"
      }
    ]
  },
  {
    "type": "post",
    "url": "/api/user/getUserByToken.json",
    "title": "根据token获取用户信息接口",
    "version": "1.0.0",
    "group": "user",
    "name": "getUserByToken",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "Token",
            "description": "<p>用户token</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "data.userId",
            "description": "<p>用户id</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "data.userName",
            "description": "<p>用户名称</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "data.kind",
            "description": "<p>类型{0: 用户, 1: 管理员}</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "code",
            "description": "<p>状态码</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "codeMsg",
            "description": "<p>状态码描述信息</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "data",
            "description": "<p>数据</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "参数异常请求返回示例:",
          "content": "HTTP/1.1 200 OK\n{\n  \"code\": 1000,\n  \"msg\": \"参数为空\"\n  \"data\": null\n}",
          "type": "json"
        }
      ]
    },
    "filename": "./src/main/java/com/manage/dms/web/api/UserController.java",
    "groupTitle": "user",
    "sampleRequest": [
      {
        "url": "http://114.80.222.219:8088/dms/api/user/getUserByToken.json"
      }
    ]
  },
  {
    "type": "post",
    "url": "/api/user/login.json",
    "title": "登录接口",
    "version": "1.0.0",
    "group": "user",
    "name": "login",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "userName",
            "description": "<p>用户名</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "password",
            "description": "<p>密码</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "data.Token",
            "description": "<p>用户token</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "code",
            "description": "<p>状态码</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "codeMsg",
            "description": "<p>状态码描述信息</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "data",
            "description": "<p>数据</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "参数异常请求返回示例:",
          "content": "HTTP/1.1 200 OK\n{\n  \"code\": 1000,\n  \"msg\": \"参数为空\"\n  \"data\": null\n}",
          "type": "json"
        }
      ]
    },
    "filename": "./src/main/java/com/manage/dms/web/api/UserController.java",
    "groupTitle": "user",
    "sampleRequest": [
      {
        "url": "http://114.80.222.219:8088/dms/api/user/login.json"
      }
    ]
  },
  {
    "type": "post",
    "url": "/api/user/updateUser.json",
    "title": "编辑用户信息接口",
    "version": "1.0.0",
    "group": "user",
    "name": "updateUser",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "Token",
            "description": "<p>用户token</p>"
          }
        ]
      }
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": true,
            "field": "userId",
            "description": "<p>用户id 为空表示添加  不为空为修改</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "password",
            "description": "<p>密码</p>"
          }
        ]
      }
    },
    "filename": "./src/main/java/com/manage/dms/web/api/UserController.java",
    "groupTitle": "user",
    "sampleRequest": [
      {
        "url": "http://114.80.222.219:8088/dms/api/user/updateUser.json"
      }
    ],
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "code",
            "description": "<p>状态码</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "codeMsg",
            "description": "<p>状态码描述信息</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "data",
            "description": "<p>数据</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "参数异常请求返回示例:",
          "content": "HTTP/1.1 200 OK\n{\n  \"code\": 1000,\n  \"msg\": \"参数为空\"\n  \"data\": null\n}",
          "type": "json"
        }
      ]
    }
  }
] });
